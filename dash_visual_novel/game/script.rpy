﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

image general = "images/spr_general.png"
image general war = "images/spr_general_war.png"
image general flip = im.Flip("images/spr_general.png", horizontal=True)
image general war flip = im.Flip("images/spr_general_war.png", horizontal=True)
image soldier = "images/spr_soldier.png"
image soldier flip = im.Flip("images/spr_soldier.png", horizontal=True)
image soldier2 = "images/spr_soldier.png"
image soldier2 flip = im.Flip("images/spr_soldier.png", horizontal=True)
image king = "images/spr_king.png"
image king happy = "images/spr_king_happy.png"
image king scared = "images/spr_king_scared.png"
image king flip = im.Flip("images/spr_king.png", horizontal=True)
image king happy flip = im.Flip("images/spr_king_happy.png", horizontal=True)
image king scared flip = im.Flip("images/spr_king_scared.png", horizontal=True)

image darklord = "images/spr_darklord.png"
image darklord annoy = "images/spr_darklord_annoy.png"
image darklord angry = "images/spr_darklord_angry.png"
image darklord flip = im.Flip("images/spr_darklord.png", horizontal=True)
image darklord annoy flip = im.Flip("images/spr_darklord_annoy.png", horizontal=True)
image darklord angry flip = im.Flip("images/spr_darklord_angry.png", horizontal=True)

image igzalio = "images/spr_igzalio.png"
image igzalio happy = "images/spr_igzalio_happy.png"
#image igzalio grin = "images/spr_igzalio_grin.png"
image igzalio cackle = "images/spr_igzalio_cackle.png"

image igzalio flip = im.Flip("images/spr_igzalio.png", horizontal=True)
image igzalio happy flip = im.Flip("images/spr_igzalio_happy.png", horizontal=True)
#image igzalio grin flip = im.Flip("images/spr_igzalio_grin.png", horizontal=True)
image igzalio cackle flip = im.Flip("images/spr_igzalio_cackle.png", horizontal=True)

image orc = "images/spr_orc.png"
image orc flip = im.Flip("images/spr_orc.png", horizontal=True)

#Other images
image spit = "images/spr_spit.png"
image dunfilter = "images/spr_dungeon_filter.png"

# Declare characters used by this game.
define hero = Character('Hero', color = "#a2f0a2")
#define nar = Character('narrator', color = "#c8ffc8")
define general = Character('General Ailith', color = "#f0a2a2")
define king = Character('King Audric', color = "#a5b3dc")
define messenger = Character('Messenger', color = "#ffffff")
define unknown = Character('???', color = "#c8ffc8")
define DarkLord = Character('Dark Lord Xeralin', color = "#bfa1d0")
define soldier = Character('Soldier', color = "#ffffff")
define Igzalio = Character('Igzalio', color = "#eec896")
define Monster = Character('Monster', color = "#ffffff")

#Scenes
image bg village = "images/bg_village.png"
image bg recruitment = "images/cg_recruitment.png"
image bg road = "images/bg_road.png"
image bg castle = "images/bg_castle.png"
image bg speech = "images/cg_speech.png"
image bg camp = "images/bg_warcamp.png"
image bg training = "images/bg_training.png"
image bg training night = "images/bg_training_night.png"
image bg forest = "images/bg_forest.png"
image bg battle = "images/cg_battle.png"
image bg dungeon = "images/bg_dungeon.png"
image bg throne = "images/cg_throne.png"
image bg hall = "images/bg_hall.png"
image bg evil = "images/bg_evil.png"
image bg human = "images/cg_human.png"
image bg ohno = "images/cg_ohno.png"
image bg dash = "images/dash.png"
image bg credits = "images/credits.png"

#Global Variables
define lacrosse = False
define shoot = False

define peaceKing = 0
define peaceDL = 0

label splashscreen:
    scene bg dash with fade
    with Pause(2)

    scene black with dissolve

    return

# The game starts here.
label start:

label SCENE1:

#Ask for name and gender here.
define gender = "f"#m or f

#conditional images
image mprotag = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_happy.png",
                                "gender == \"m\"", "images/spr_mprotag.png",
                                "gender == \"f\"", "images/spr_fprotag.png")
image mprotag happy = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_cackle.png",
                                        "gender == \"m\"", "images/spr_mprotag_happy.png",
                                        "gender == \"f\"", "images/spr_fprotag_happy.png")
image mprotag unhappy = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio.png",
                                        "gender == \"m\"", "images/spr_mprotag_unhappy.png",
                                        "gender == \"f\"", "images/spr_fprotag_unhappy.png")
image mprotag concern = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_happy.png",
                                        "gender == \"m\"", "images/spr_mprotag_concern.png",
                                        "gender == \"f\"", "images/spr_fprotag_concern.png")
image mprotag angry = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_happy.png",
                                        "gender == \"m\"", "images/spr_mprotag_angry.png",
                                        "gender == \"f\"", "images/spr_fprotag_angry.png")
image mprotag flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_happy.png", horizontal=True),
                                    "gender == \"m\"", im.Flip("images/spr_mprotag.png", horizontal=True),
                                    "gender == \"f\"", im.Flip("images/spr_fprotag.png", horizontal=True))
image mprotag happy flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_cackle.png", horizontal=True),
                                            "gender == \"m\"", im.Flip("images/spr_mprotag_happy.png", horizontal=True),
                                            "gender == \"f\"", im.Flip("images/spr_fprotag_happy.png", horizontal=True))
image mprotag unhappy flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio.png", horizontal=True),
                                            "gender == \"m\"", im.Flip("images/spr_mprotag_unhappy.png", horizontal=True),
                                            "gender == \"f\"",  im.Flip("images/spr_fprotag_unhappy.png", horizontal=True))
image mprotag concern flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_happy.png", horizontal=True),
                                            "gender == \"m\"", im.Flip("images/spr_mprotag_concern.png", horizontal=True),
                                            "gender == \"f\"",  im.Flip("images/spr_fprotag_concern.png", horizontal=True))
image mprotag angry flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_happy.png", horizontal=True),
                                            "gender == \"m\"", im.Flip("images/spr_mprotag_angry.png", horizontal=True),
                                            "gender == \"f\"", im.Flip("images/spr_fprotag_angry.png", horizontal=True))

image mprotag a = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_happy.png",
                                    "gender == \"m\"", "images/spr_mprotaga.png",
                                    "gender == \"f\"", "images/spr_fprotaga.png")
image mprotag a happy = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_cackle.png",
                                        "gender == \"m\"", "images/spr_mprotaga_happy.png",
                                        "gender == \"f\"", "images/spr_fprotaga_happy.png")
image mprotag a unhappy = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio.png",
                                            "gender == \"m\"", "images/spr_mprotaga_unhappy.png",
                                            "gender == \"f\"", "images/spr_fprotaga_unhappy.png")
image mprotag a concern = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_happy.png",
                                            "gender == \"m\"", "images/spr_mprotaga_concern.png",
                                            "gender == \"f\"", "images/spr_fprotaga_concern.png")
image mprotag a angry = ConditionSwitch("pname == \"Igzalio\"","images/spr_igzalio_happy.png",
                                        "gender == \"m\"", "images/spr_mprotaga_angry.png",
                                        "gender == \"f\"", "images/spr_fprotaga_angry.png")
image mprotag a flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_happy.png", horizontal=True),
                                        "gender == \"m\"", im.Flip("images/spr_mprotaga.png", horizontal=True),
                                        "gender == \"f\"", im.Flip("images/spr_fprotaga.png", horizontal=True))
image mprotag a happy flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_cackle.png", horizontal=True),
                                            "gender == \"m\"", im.Flip("images/spr_mprotaga_happy.png", horizontal=True),
                                            "gender == \"f\"", im.Flip("images/spr_fprotaga_happy.png", horizontal=True))
image mprotag a unhappy flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio.png", horizontal=True),
                                                "gender == \"m\"", im.Flip("images/spr_mprotaga_unhappy.png", horizontal=True),
                                                "gender == \"f\"",  im.Flip("images/spr_fprotaga_unhappy.png", horizontal=True))
image mprotag a concern flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_happy.png", horizontal=True),
                                                "gender == \"m\"", im.Flip("images/spr_mprotaga_concern.png", horizontal=True),
                                                "gender == \"f\"",  im.Flip("images/spr_fprotaga_concern.png", horizontal=True))
image mprotag a angry flip = ConditionSwitch("pname == \"Igzalio\"",im.Flip("images/spr_igzalio_happy.png", horizontal=True),
                                                "gender == \"m\"", im.Flip("images/spr_mprotaga_angry.png", horizontal=True),
                                                "gender == \"f\"", im.Flip("images/spr_fprotaga_angry.png", horizontal=True))

image fprotag = ConditionSwitch("gender == \"f\"", "images/spr_mprotag.png", "gender == \"m\"", "images/spr_fprotag.png")
image fprotag happy = ConditionSwitch("gender == \"f\"", "images/spr_mprotag_happy.png", "gender == \"m\"", "images/spr_fprotag_happy.png")
image fprotag unhappy = ConditionSwitch("gender == \"f\"", "images/spr_mprotag_unhappy.png", "gender == \"m\"", "images/spr_fprotag_unhappy.png")
image fprotag concern = ConditionSwitch("gender == \"f\"", "images/spr_mprotag_concern.png", "gender == \"m\"", "images/spr_fprotag_concern.png")
image fprotag angry = ConditionSwitch("gender == \"f\"", "images/spr_mprotag_angry.png", "gender == \"m\"", "images/spr_fprotag_angry.png")
image fprotag flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotag.png", horizontal=True), "gender == \"m\"", im.Flip("images/spr_fprotag.png", horizontal=True))
image fprotag happy flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotag_happy.png", horizontal=True), "gender == \"m\"", im.Flip("images/spr_fprotag_happy.png", horizontal=True))
image fprotag unhappy flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotag_unhappy.png", horizontal=True), "gender == \"m\"",  im.Flip("images/spr_fprotag_unhappy.png", horizontal=True))
image fprotag concern flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotag_concern.png", horizontal=True), "gender == \"m\"",  im.Flip("images/spr_fprotag_concern.png", horizontal=True))
image fprotag angry flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotag_angry.png", horizontal=True), "gender == \"m\"", im.Flip("images/spr_fprotag_angry.png", horizontal=True))

image fprotag a = ConditionSwitch("gender == \"f\"", "images/spr_mprotaga.png", "gender == \"m\"", "images/spr_fprotaga.png")
image fprotag a happy = ConditionSwitch("gender == \"f\"", "images/spr_mprotaga_happy.png", "gender == \"m\"", "images/spr_fprotaga_happy.png")
image fprotag a unhappy = ConditionSwitch("gender == \"f\"", "images/spr_mprotaga_unhappy.png", "gender == \"m\"", "images/spr_fprotaga_unhappy.png")
image fprotag a concern = ConditionSwitch("gender == \"f\"", "images/spr_mprotaga_concern.png", "gender == \"m\"", "images/spr_fprotaga_concern.png")
image fprotag a angry = ConditionSwitch("gender == \"f\"", "images/spr_mprotaga_angry.png", "gender == \"m\"", "images/spr_fprotaga_angry.png")
image fprotag a flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotaga.png", horizontal=True), "gender == \"m\"", im.Flip("images/spr_fprotaga.png", horizontal=True))
image fprotag a happy flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotaga_happy.png", horizontal=True), "gender == \"m\"", im.Flip("images/spr_fprotaga_happy.png", horizontal=True))
image fprotag a unhappy flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotaga_unhappy.png", horizontal=True), "gender == \"m\"",  im.Flip("images/spr_fprotaga_unhappy.png", horizontal=True))
image fprotag a concern flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotaga_concern.png", horizontal=True), "gender == \"m\"",  im.Flip("images/spr_fprotaga_concern.png", horizontal=True))
image fprotag a angry flip = ConditionSwitch("gender == \"f\"", im.Flip("images/spr_mprotaga_angry.png", horizontal=True), "gender == \"m\"", im.Flip("images/spr_fprotaga_angry.png", horizontal=True))

    #Other effects
    #if Friend is male, he says "Oh crap" instead of "Oh my gosh", spits from different locations, and female hero scolds male friend for calling her heavy

#BACKSTORY!:
#<Words appear on entire screen, rather than in text box at base. And the first four statements each go on their own lines.>
label prologue:
    $ save_name = "Prologue"
    stop music fadeout 1.0
    play music "music/bgm_prologue.MP3"
    scene black with dissolve
    show text "{size=+10}Two kingdoms at war.{/size}" with dissolve
    $ renpy.pause(3)
    scene black with dissolve
    show text "{size=+10}Larien and Deragoth.{/size}" with dissolve
    $ renpy.pause(3)
    scene black with dissolve
    show text "{size=+10}Humans and Monsters.{/size}" with dissolve
    $ renpy.pause(3)
    scene black with dissolve
    show text "{size=+10}Good and Evil." with dissolve
    $ renpy.pause(3)
    scene black with dissolve
    show text "{size=+10}Years ago, the previous Dark Lord Zerilan declared war on the peaceful human kingdom of Larien by sending massive armies of monsters to invade.{/size}" with dissolve
    $ renpy.pause(13)
    scene black with dissolve
    show text "{size=+10}The then King Aurous sent his own troops to repel them and tried to make peace with Deragoth before conditions worsened, but his attempts failed.{/size}" with dissolve
    $ renpy.pause(13)
    scene black with dissolve
    show text "{size=+10}In the many years since then, the war has been in careful balance, with no side having a clear advantage.{/size}" with dissolve
    $ renpy.pause(13)
    scene black with dissolve
    show text "{size=+10}Although the changing times led to Dark Lord Xeralin and King Audric succeeding their fathers, the war is one thing that continues to remain the same.{/size}" with dissolve
    $ renpy.pause(13)
    scene black with dissolve
    show text "{size=+10}In a small, unremarkable town in Larien, a villager ponders these events.{/size}" with dissolve
    $ renpy.pause(7)
    scene black with dissolve
    show text "{size=+10}That villager…is you.{/size}" with dissolve
    $ renpy.pause(3)
    scene black with dissolve
    show text "{size=+10}Although you have never experienced the war firsthand, you know that it has been raging since you were very young.{/size}" with dissolve
    $ renpy.pause(8)
    scene black with dissolve
    show text "{size=+10}You wonder…will this war ever end?{/size}" with dissolve
    $ renpy.pause(5)
    
    stop music fadeout 1.0
    #    (After War and Backstory is explained to player…)
    scene black with dissolve
    $ pname = "initialized"

    show mprotag flip at left
    show fprotag at right
    menu:
        "Are you a girl or a boy?"
        "Girl":
            $ gender = "f"
            hide fprotag
            show mprotag at center with move
            $ shehe = "he"
            $ herhim = "him"
            $ herhis = "his"

            $ SheHe = "He"
            $ HerHim = "Him"
            $ HerHis = "His"

            $ fname = "Jack"

        "Boy":
            $ gender = "m"
            hide fprotag
            show mprotag at right
            show mprotag at center with move
            $ shehe = "she"
            $ herhim = "her"
            $ herhis = "her"

            $ SheHe = "She"
            $ HerHim = "Her"
            $ HerHis = "Her"

            $ fname = "Jill"

    $ friend = Character(fname, color = "#f1e5a0")


label zeroLength:
    $ grumpCount = 0
    if gender == "f":
        $ pname = renpy.input("Enter the name that you want for the main character", "Jill")
    else:
        $ pname = renpy.input("Enter the name that you want for the main character", "Jack")
        
    $ pname = pname.strip()
    if len(pname.decode("utf-8")) == 0:
        jump zeroLength
    
    if pname == "Igzalio":
        $ gender = "m"
        $ shehe = "she"
        $ herhim = "her"
        $ herhis = "her"

        $ SheHe = "She"
        $ HerHim = "Her"
        $ HerHis = "Her"

        $ fname = "Jill"
    
    $ hero = Character(pname, color = "#c8ffc8")
    scene black with dissolve
    show text "{size=+10}Chapter 1\nAn Adventure Begins...{/size}" with dissolve
    $ save_name = "Chapter 1: An Adventure Begins..."
    $ renpy.pause(1.5)
    scene black with dissolve

    scene bg village with fade
    show mprotag at left
    play music "music/bgm_village.MP3"
    friend "Hey! Heeeyy! %(pname)s! Wait up!"
    #    <Play running sound effect, Enter Friend. Hero turns toward Friend. Friend smile face.>
    show mprotag flip
    play sound "sounds/snd_run.wav"
    show fprotag happy at right with moveinright

    "You turn at the sound of the familiar voice and watch as [fname], your childhood friend, charges up to your side. [SheHe] is slightly out of breath from [herhis] efforts, but is wearing an enthusiastic grin."
    stop sound  fadeout 1.0
    friend "I called out to you probably two million times! Why didn’t you answer? For a second there I thought that you were trying to avoid me…"
    menu:
            friend "But I know that you would never do that!"
            "Sorry. I was caught up in my own thoughts.":
                $ grumpCount=grumpCount-1
                #hero "Sorry. I was caught up in my own thoughts."
                show fprotag at right
                friend "Oh. What were you thinking about? What’s going to be at the market? What you are going to get me for my birthday next week? The war?"
            "Well, I was avoiding you! Obviously!":
                $ grumpCount=grumpCount+1
                show mprotag happy flip
                #hero "Well, I was avoiding you! Obviously!"
                show fprotag angry
                friend "Hey! That’s not very nice. You were the one who wanted to go to the market today."
            "You know, I was in the middle of thinking. You didn’t have to interrupt me.":
                $ grumpCount=grumpCount+1
                #hero "You know, I was in the middle of thinking. You didn’t have to interrupt me."
                show fprotag angry
                friend "Well. Sorry. Why are you being such a grouch today?"
                show fprotag concern
                friend "Couldn’t sleep last night?"
    "Your friend opens [herhis] mouth to pester you more, but a commotion from the town square silences [herhim]."
    "You both look to see a mass of people congregating around a raised platform. A woman outfitted in full plate armor is trying to address the roaring crowd."

    scene bg recruitment with fade:
        zoom 0.5
    play music "music/bgm_inspire.MP3"
    play sound "sounds/snd_cheer.wav"
    "Stern Woman" "Calm down, people! Yes, I understand your enthusiasm, but please try to be orderly about this…"
    show mprotag flip at left
    show fprotag at right
    menu:
        friend "Huh. I wonder what could be going on. Let’s go check it out!"
        "Okay!":
            show mprotag happy flip
            "You hurry over with [fname] to investigate."
        "I don’t think that’s a good idea…":
            show mprotag concern flip
            show fprotag happy
            "Laughing at your reluctance, [fname] grabs your wrist and pulls you along anyway."

    play sound "sounds/snd_run.wav"
    show fprotag happy at center with move
    show fprotag happy flip at center
    "However, since you are at the back, it is still practically impossible to see what the fuss is about."
    
    stop sound fadeout 1.0
    show mprotag flip
    show fprotag flip
    friend "Come on, we need to get to the front. Excuse me..."
    play sound "sounds/snd_hit.wav"
    with vpunch
    show fprotag flip at right with move
    show mprotag flip at center with move
    friend "Pardon me…"
    play sound "sounds/snd_hit.wav"
    with hpunch
    "With plenty of shoving to accompany [herhis] apologies, [fname] forces [herhis] way to the front, with you squeezing in after [herhim]. Now that you are close enough to the speaker, you can see that she bears the royal crest on her armor."
    "She must be a general of the King’s Army! But what is she doing in your humble village…?"
    
    hide mprotag
    hide fprotag
    scene bg recruitment at Position (xpos = 500, xanchor = 0.5, ypos = 375, yanchor = 0.5):
        zoom 0.5
        easein 1.0 zoom 1.0
        
    #show general at center
    stop sound fadeout 1.0
    general "Thank you all for coming to listen to me today. I am General Ailith, from the King’s Army. I am here to speak with you about the ongoing war with the monsters."
    general "Every day, we must fight these vile creatures to protect our lands, our livelihoods, and our families and friends."
    general "Being in a small village with little experience in fighting must make each of you feel like there is nothing you can do to help our great kingdom repel the creatures of darkness."
    general "However, you can. The power to remedy your ignorance with the blade is at hand! Return to the capital with us, and we will equip you with the knowledge and the resources to make a difference." 
    general "I implore you, come with us, and fight! For your family! For your friends! Be the hero who will bring peace to our noble kingdom!"
    play sound "sounds/snd_cheer.wav"
    #hide general
    
    scene bg recruitment at Position (xpos = 500, xanchor = 0.5, ypos = 375, yanchor = 0.5):
        zoom 1.0
        easein 1.0 zoom 0.5
    
    "At this, the crowd went up in a cheer, with [fname] as one of the loudest voices."
    
    show mprotag flip at left
    show fprotag happy at right
    friend "A chance to fight in the war? This is so exciting! What do you think, %(pname)s?"
    menu:
        friend "Are you interested in joining? I am! I wonder where we sign up."
        "Yes! Let’s do this!":
            show mprotag happy flip
            hero "It will be so cool to fight the monsters! I can’t wait! Do you think that we'll be war heroes, and get to live at the palace? That would be much more exciting than staying here forever!"
            friend "Hah! War heroes? I doubt it! But...that would be cool. And it would be awesome if we ended up making a bit of a difference out there. Defending our lands will be such a great adventure!" 
        "War? Fighting? I don’t think so.":
            $ grumpCount=grumpCount+1
            show mprotag unhappy flip
            hero "Think about it. We don’t have any experience now. What if we still aren’t ready to fight by the time they send us out? And in our village, we aren’t exactly affected by the war. Let’s keep it that way."
            show fprotag concern
            if grumpCount == 2:
                friend "Come on! Quit being so grumpy!"
            friend "%(pname)s, you have to be more optimistic! I’m sure that the King’s Army doesn’t send out fresh recruits who know nothing more than how to hold a weapon in their hands. Of course we have to fight!"
            scene bg ohno with dissolve
            friend "Our village might not be threatened right now, but if the war doesn’t stop soon, it will reach us eventually. Ignoring it won’t make it go away."
            pause (0.5)
            scene bg recruitment at Position (xpos = 500, xanchor = 0.5, ypos = 375, yanchor = 0.5) with dissolve:
                zoom 0.5
            show mprotag flip at left
            show fprotag at right
            hero "You're right. I guess I wasn't thinking."
        "I don’t know…I guess if you're going to join, then why not…?":
            show mprotag concern flip
            hero "You are my best friend, after all. If you are dead set on going, then I suppose we should stick together. I don’t really have a good reason not to sign up, anyway…"
            show fprotag concern
            friend "Well, you certainly sound enthusiastic about this. You look like you just got assigned to mucking out stables for an entire year."
            show fprotag happy
            friend "Get excited! We'll protect the innocent, and it will be so epic!"

    "Feeling inspired by [fname], you follow [herhim] as [shehe] leads the crowd to where General Ailith is waiting for interested villagers."
    
    #    <Friend happy face, Hero neutral face, Friend and Hero on one side of screen, General on other, all facing each other, Friend in front of Hero>
    show mprotag flip at left
    show fprotag flip at center with move
    show general at right with moveinright
    general "You intend on becoming a soldier for the King’s Army?"
    friend "Yes! Both of us!"
    general "Very well, then. Just put your names here and be ready to depart with the rest of us at first light tomorrow. There will be a long march back to the capital, with very few breaks."
    general "Do not bring too many possessions, as they will weigh you down. If you do not believe that you can handle this, then you are not fit to be a soldier."
    friend "I’m game! That doesn’t sound too bad."
    hide general with moveoutright
    "[fname] signs [herhis] name to the paper, and you follow suit. You want to stay and talk to the General more, but are shooed away by her accompanying soldiers to make more space for other potential recruits."
    
    scene bg village with fade
    stop sound fadeout 1.0
    stop music fadeout 1.0
    play music "music/bgm_village.MP3"
    show mprotag concern flip at left
    show fprotag happy flip at center
    "Tomorrow morning… that will be the day when your new chapter begins. Looking around at the buildings, you realize that you will not see any of them for a very long time. The thought is rather sobering."
    "You are snapped out of your reverie, however, when [fname] speaks again."
    show fprotag happy
    friend "Hey, I just thought of something! We are going to the capital, where the castle is! Do you think that we will get to see the king?"
    show fprotag concern
    if gender == 'm':
        friend "I wonder what he will look like. And what he will be wearing."
    else:
        friend "I wonder if he knows how to use a sword. I bet his would have a lot of gems on it." 
    show fprotag happy
    friend "Oh! What if…"
    "[fname] continues [herhis] cheerful small talk as you listen in silence. [HerHis] obvious excitement bolsters your spirits, but you still cannot completely shake that ominous feeling of change about to occur."
    "It follows you throughout the day, and you find it difficult to enjoy your last day at home."

###################################################################################################################################################################################################
label SCENE2:
    stop sound
    stop music fadeout 1.0
    scene black with dissolve
    show text "{size=+10}Chapter 2\nIn the Army{/size}" with dissolve
    $ save_name = "Chapter 2: In the Army"
    $ renpy.pause(1.5)
    scene black with dissolve
    
    play music "music/bgm_village.MP3"
    
    play sound "sounds/snd_knock.wav"
    "KNOCK! KNOCK!"
    "Someone pounds on your door. You had just gathered the few possessions that you decided were the most important and put them in a small pack. Slinging the strap over your shoulder, you open the door to find..."

    scene bg village with fade
    show fprotag happy at right with dissolve
    show mprotag flip at left with moveinleft
    #play music "music/bgm_village.MP3"
    friend "Good morning! I was just making sure that you are awake. You do have a tendency to sleep in."
    friend "Oh, and I see you have gotten your things together! Me too! Are you ready to go? I don’t want the General to leave because she thinks we changed our minds! Hurry! We should have been there already!"
    "Your friend’s enthusiasm is infectious, and you hustle out of your house, making sure to lock the door behind you. Who knows how long it will be until you are home next?"
    
    show fprotag flip at center with move
    play sound "sounds/snd_run.wav"
    "You make your way over to a small crowd of people. They are obviously the other recruits. They are holding their small bags of possessions tightly, but their eyes are shining with excitement."
    stop sound  fadeout 1.0

    show mprotag flip at left
    show fprotag flip at center
    general "Is everybody here? We are leaving in a few minutes. We have a tight schedule to keep, and cannot afford to wait for stragglers who slept in late."
    show fprotag happy flip
    friend "Oh, wow. This is so cool! I can’t wait to start!"
    menu:
        friend "What about you, %(pname)s? You’re excited too, right?"
        "Of course! How could I not be?":
            show mprotag happy flip
            friend "I thought so! I can read you like a book! I guess that is what happens when you grow up knowing someone for your whole life."
        "Actually, I’m more nervous than anything.":
            show mprotag concern flip
            show fprotag flip
            friend "Really? Well…I guess that I am a little nervous too. But it will be great! Just wait and see. There will be nothing to worry about."
        "Let’s do this! With us working together, there’s nothing we can’t do!":
            show mprotag happy flip
            friend "Exactly! Let’s go and learn how to kill some of those evil monsters!"
    
    "A few more people come running up to the group from the village. Part of you is happy that you and [fname] were not the last ones to show up. General Ailith seems unimpressed at the newcomers' tardiness."
    general "It appears that no one else will be coming. We start out now. If you can’t handle the pace, go home."
    "With her short proclamation concluded, the General mounts a horse and urges it to begin trotting away. The soldiers that had come with her scramble into a formation as they follow her. You and the other fresh recruits fall in behind them."
    stop music fadeout 1.0
    scene black with fade
    play music "music/bgm_army.MP3"
    #renpy.music.register_channel("loopsound", "sfx", True)
    #I defined in options.rpy for looping sounds
    play loopsound "sounds/snd_march.wav"
    scene bg road with fade
    
    "It is a long trek, lasting several days. At first, there are a fair amount of breaks. But as the days pass, the breaks become less frequent, and the General stops later and starts earlier."
    "About half of the recruits cannot handle this, and break off to go home. Today, based on the position of the sun, it seems that you have been walking for several hours without a break yet."
    "The exertion is beginning to show in all of the recruits’ faces. Stealing a glance at the soldiers, you find that they seem as stoic as ever. If they are struggling, there is no sign."
    show mprotag concern flip at left
    show fprotag unhappy flip at center
    show soldier flip at right
    friend "Uuuuuggggghhhh…The sun is so hot! I must have sweat all the way through my shirt!"
    menu:
        friend "Are you feeling this miserable, too? When do you think we will be taking a break?"
        "Can’t…keep…up…too…exhausted…":
            show mprotag concern flip
            hero "This march is brutal. If training will be this hard… I guess that we will have to adapt quickly."
            show fprotag concern flip
            friend "…I guess that you are right. I can’t drop out now! Especially not if you are going to keep moving. I can’t do worse than you!"
        "We can make it through this.":
            hero "Keep going, [fname]. I’m sure that the General will call a break soon. She can’t expect us to keep up forever."
            show fprotag flip
            friend "Thanks, %(pname)s. I sure hope that you are right. Dropping unconscious from overexertion in the middle of a crowd would be embarrassing!"
        "There will be no breaks!":
            show mprotag angry flip
            hero "We will walk straight through the night to reach the capital! Why else would this be considered part of the recruitment process? The other days were just warm-ups!"
            show fprotag angry
            friend "WHAT?!?"
            show fprotag concern flip
            friend "Oh…wait, you are joking, right? Please tell me that was a joke. How would you even get that information, anyway? You can’t be serious."
            show mprotag flip
    #stop sound footsteps
    stop loopsound
    hide mprotag
    hide fprotag
    hide soldier
    "Suddenly, everyone stops. Hoping that the General is allowing a break, you gladly halt in your tracks. If it isn’t a break…you have no idea how you will start moving again."
    show general at right
    general "We have almost reached the capital. If we continue at this pace, it will be less than an hour until our arrival."
    "This announcement is met with a lot of thankful murmuring amongst the recruits. There is a moment of small talk as the recruits all exchange ideas of what they think the capital will look like."
    general "Okay, you lot, let’s get moving. This break has already been longer than I planned. I, for one, am not going to arrive after dark."
    hide general
    "You groan as you wearily start moving again. How could that have possibly been considered a break? It was so short. Much shorter than any other break thus far."
    
    play loopsound "sounds/snd_march.wav"
    "From there, the journey is uneventful, with the exception of recruits muttering grievances to themselves. Even though you know that you will be there soon, your body wants to stop."
    "Finally, finally, the capital comes into sight. The entirety of the party seems to be reinvigorated by this. Even the General seems relieved to be so near."
    "As you grow closer, you can hear a roaring noise. It takes a minute, but you finally realize that it is people cheering from the capital, and it is so loud that you can hear it from outside."
    show mprotag flip at left
    show fprotag flip at center
    show soldier flip at right
    play sound "sounds/snd_cheer.wav"
    menu:
        "What could be going on? You resolve to ask someone."
        "Ask [fname].":
            hero "Hey, [fname], what do you think is going on?"
            show fprotag happy
            friend "I have no idea! Maybe it’s for us? That would be so cool!"
        "Ask a fello recruit.":
            show mprotag
            hero "Do you know what that cheering is supposed to be for?"
            "Recruit" "How should I know? Go bother someone else."            
        "Ask a soldier.":
            hero "Excuse me, but what are those people cheering for?"
            show soldier
            soldier "The king is giving a speech today to all of the new recruits. It must be that he’s going to start soon. We are probably running late, then."
    "You can’t speak anymore, because the General has urged her mount to move faster, and you must tap into all your energy reserves in order to keep up."
    scene black with fade
    "When you enter the capital, you are struck dumb by the sheer size and majesty of everything. Even the residential buildings seem impossibly grander than anything you have ever seen in your village."
    "All of the recruits, including [fname], are wearing an expression of awe that mirrors how you feel inside."
    stop loopsound fadeout 1.0
    scene bg castle with fade
    play music "music/bgm_regal.MP3"
    show mprotag flip at left
    show fprotag happy at right
    "However, General Ailith and the soldiers herd you quickly though the city, not giving you much time to take in anything. The cheering is growing louder. You must be drawing closer to its source…"
    "Suddenly, you find yourself in front of the largest building you have ever seen. It towers above all the others, and flags displaying the royal crest snap in the breeze from atop the towers."

    $SillyProtag = True
    menu:
        "There is only one thing it could possibly be…"
        "It's a restaurant!":
            pass
        "That is a huge inn!":
            pass
        "It's the castle!":
            $SillyProtag = False
        "Wow, an indoor village!!":
            pass
    show mprotag happy flip
    if SillyProtag:
        show fprotag concern at right
        friend "Um...%(pname)s? You know that that is the castle, right? Like, the place where the king lives? Speaking of which…"
        show fprotag happy at right
        friend "Is that him? There, on that balcony!!"
    else:
        show fprotag happy at right
        friend "I know! I can’t believe it…and wait, who is that up there? On the balcony? It’s a bit too far away to see clearly, but that must be the king!!"
    show bg speech with dissolve    
    "You look to where [fname] points, and indeed, there is a figure standing on the balcony of the castle. Even from this distance, you can see that he is resplendent in dazzling robes and a shining crown."
    
    general "Oh, what a relief. So he hasn’t started speaking yet. We made it just in time."
    king "As your one and only King Audric, I am here to welcome you. So welcome, one and all! I am pleased to announce that you have volunteered to fight in the war! You are the new recruits!"
    king "Soon, you shall begin your training, and once ready, will join the fight to rid our fair kingdom, nay, the world, of these vile monsters! They are a contaminant in our pure lands, and deserve to be destroyed!!"
    king "We cannot continue to exist while they plague us with their evil presence."
    king "And so, in order to survive, their eradication is of the utmost importance! We shall send them scuttling back into their dark holes with their tails between their legs!"
    king "But that is the worst case scenario. In the best case, we shall annihilate them all! Their terrorism will end by your hand! Be the hero our kingdom needs!"
    play sound "sounds/snd_cheer.wav"
    "After this inspirational speech, the entire crowd cheers. You add to the din yourself, although something about that speech didn’t seem right, and you feel apart from the charged atmosphere."
    "Before you can give any more thought to it, the General, now dismounted from her horse, collects all of the recruits, even ones from other recruitment efforts, and directs them all towards the garrison where the soldiers-in-training will sleep."
    "You become a part of the thick flow, unable to change direction until you arrive at the destination."
    stop sound fadeout 1.0
    scene bg training night with fade
    show general at right
    show mprotag flip at left
    show fprotag flip at center
    stop music fadeout 1.0
    play music "music/bgm_army.MP3"
    general "This is where you will be living. Men will be over here, and women over there. Please go and claim your bed."
    general "That will be all for today, but tomorrow be ready for your intense training to begin. Life as a soldier will not be easy, but I expect you all to put in the effort."
    hide general with moveoutright
    "The General departs, leaving all of the recruits to their own devices. Many of your peers run off to their respective garrisons in order to get the best bed available."
    show fprotag unhappy
    friend "Well, I guess this is it. Who knows exactly what training will involve?"
    menu:
        friend "I hope that we will get to see each other often. I don’t know what I would do if I never saw you…"
        "I hope so too!":
            show mprotag happy flip
            show fprotag
            friend "Yeah…I guess that we will find out tomorrow."
            friend "Oh, almost all of the others have gone into the garrisons. We are going to get the leftover beds that nobody wants now! We should probably go claim ours, before we are left with the worst of the worst."
        "I know what I would do: party!":
            show mprotag happy flip
            show fprotag angry
            friend "%(pname)s! That isn’t funny! If you’re going to act like that, then I am going to go get my bed before there is nothing decent left."
    show fprotag concern
    friend "I guess that I’ll see you around, then. Hopefully."
    
    hide mprotag
    hide fprotag
    "You say your goodbyes, and nervously head over to the garrison. You feel exposed without your friend’s constant presence next to you. Luckily, you manage to find a bed in decent shape in an overlooked corner."
    "You stake your claim by collapsing onto it. Still feeling depleted of energy from the march to the capital, you are too tired to participate the recruits' mingling. You drift off to sleep even as you finish the thought…"
    scene black with fade
    with Pause(2)
    #    <Black Screen for a few seconds.>
########################################################################################################################################################################
label SCENE3:
    stop sound
    stop music fadeout 1.0
    scene black with dissolve
    show text "{size=+10}Chapter 3\nWAR!{/size}" with dissolve
    $ save_name = "Chapter 3: WAR!"
    $ renpy.pause(1.5)
    scene black with dissolve
    
    play music "music/bgm_army.MP3"
    #    <Still in black screen.>
    general "WE MEET FOR THE FIRST EXERCISE IN FIVE MINUTES RIGHT OUTSIDE THE GARRISON!!! BE THERE OR DROP OUT!!!!!!"
    scene bg training with fade
    "And so your training begins. There are basic exercises with several weapons, such as spears and the bow. After a few days, the recruits are evaluated and given more intensive training in the weapon of which they are deemed the most skilled."
    "You are evaluated to have the most skill at the bow, and [fname] is as well. Shortly after this time, the General leaves to return to the front and guide her soldiers personally."
    "Training for weekss, you become able to use bows with a longer range, and hit the center of the target fairly often. You are pleased with your progress, but you know that there is no way that you will be sent out to fight anytime soon."
    "Until one day…"
    play sound "sounds/snd_run.wav"
    show fprotag at center    
    #    <Footstep SE, Soldier moves across screen to Friend in center of screen, who is facing opposite direction> 
    messenger "Let me through! I must speak with the king! It’s urgent!!"    
    show soldier at center with moveinright
    play sound "sounds/snd_hit.wav"
    with hpunch
    show fprotag flip at left with move
    hide soldier with moveoutleft
    show fprotag angry
    show mprotag concern at right with moveinright

    #    <Messenger to center of screen, hpunch, Friend knocked back to other side of screen, Messenger continues off to same side of screen. Enter Hero concerned face from where messenger started> 
    stop sound fadeout 1.0
    friend "Ow! What was that for? Watch where you’re going! I don’t care if you have to see the king or not…oh, well."
    show fprotag flip
    friend "He’s gone."
    show fprotag angry flip 
    friend "Seriously, we get five minutes of free time, and then this guy practically knocks me over? Who does he think he is?"
    "Training Master" "OKAY, BREAK’S OVER!! GET BACK TO WORK, SOLDIERS!!! REMEMBER, GO TO THE FIGHT, DON’T LET THE FIGHT COME TO YOU!!!"
    show fprotag flip
    friend "He needs a new catchphrase. And really, what does he mean by letting the fight come to us?"
    friend "It isn’t like the General is going to find herself in dire straits and need backup from the recruits, even though we don’t know how to fight well yet. That is totally unrealistic."
    "You reluctantly pick up your bow and return to the shooting range. Your friend follows suit, picking a target several down from yours. However, before you can even fire a single arrow, the soldier from before returns, with…the king?!?"
    
    stop music fadeout 0.5
    hide mprotag
    hide fprotag
    show soldier at left with moveinright
    show soldier flip
    show king at center with moveinright
    play music "music/bgm_tension.MP3"
    show soldier2 at right with moveinright
    king "Soldiers! Stop your practicing and listen! A terrible situation has arisen. General Ailith has been fighting on the front, but her forces are growing thin, and the monsters are strong."
    king "Although you have not yet mastered the art of fighting… you shall be going to aid her forces! Your time to become a hero is now!" 
    king "Slay the enemies with your new abilities! I believe in you! Several months of training should be enough for you to hold your own!"
    "You and [fname] exchange dumbstruck looks from across the training ground."
    king "Well? What are you waiting for? The General needs you now. Hurry! You must leave at once!"
    show soldier
    hide soldier with moveoutleft
    hide king with moveoutleft
    hide soldier2 with moveoutleft
    "Suddenly, the training grounds erupt into pandemonium as the soldiers-in-training try to figure out what to do in order to become prepared to leave for the war front."
    menu:
        "What will you do?"
        "I’m not ready to fight! I need to get out of here!":
            "You start to bolt for the nearest escape, which several other people are actually doing as well, but stop in your tracks before you make it far, thinking of [fname], who would do what was necessary despite being underprepared and frightened."
            "Feeling ashamed of your earlier thoughts, you run to get armor and extra arrows."
        "Well, I have my weapon…I need to get my armor.":
            "Without armor, you will be easy for any potential enemies to cut down. You run past several trainees who are sprinting for the exits, fleeing before the real test. Hopefully [fname] is getting [herhis] armor as well."
        "Find [fname].":
            "You glance around desperately in the pandemonium, ignoring the people trying to desert. [SheHe] will most certainly not be among them."
            "Finally, you see [herhim] over by the armory, trying to get in to get armor and a quiver of arrows, rather than the three arrows issued to trainees. Deciding that [shehe] has the right idea, you run over to the armory as well."
    
    "The armory is swarming with panicked trainees. The Training Master is yelling above the din, trying to create order, but very few people are listening to him. Even though you want to follow his orders, you can’t quite make out the words, anyway."
    "Wading through the trainees, you find yourself where you want to be: at the armor racks. You select armor that you think would fit. There is not enough room to try it on at the moment, so you turn to leave…and bump into [fname]!"
    show fprotag concern flip at right
    show mprotag concern flip at left with moveinleft
    show fprotag concern
    if gender == "m":
        friend "%(pname)s! Oh my gosh, this is crazy! Tell you what, wait for me right outside. I’ll be there in a sec. I need to find some armor and arrows."
    else:
        friend "%(pname)s! Oh crap, this is crazy! Tell you what, wait for me right outside. I’ll be there in a sec. I need to find some armor and arrows."
    "You offer a hasty agreement as you are pushed apart by the crush of people."
    hide fprotag with moveoutright
    "Before you know it, you have been expelled from the armory. Other people trickle out, but [fname] is not among them."
    "While you wait, you try to figure out where the prepared recruits should gather. You observe that the messenger from earlier is hovering around a few wagons, which are being filled with food."
    "At least some planning was going on."
    show fprotag at right with moveinright
    friend "Okay, I think I’m ready. I have my armor. I have a whole quiver of arrows. Now all we have to do is find enough food to last—oh, good! It looks like supplies are being packed over there! Perfect!" 
    friend "Other than that, I just need to figure out how to get this armor on. Hmm… maybe if I…"
    show fprotag unhappy
    friend "No, wait, what about…"
    show fprotag angry
    #friend "How is this armor supposed to work? %(pname)s, can you help? I think that this must need more than one person!"
    menu:
        friend "How is this armor supposed to work? %(pname)s, can you help? I think that this must need more than one person!"
        "Sure. Let’s see…":
            show mprotag flip
            show fprotag
            "Between you and [fname] working together, you finally figure out how to put the armor on. Once [fname] is equipped, [shehe] helps you get ready. Soon, you are both decked out in the armor of an archer."
        "I’m sure that you can do it yourself.":
            show fprotag concern
            "Giving you an exasperated glace, [fname] tries again, to no avail. You try to put on your armor yourself, thinking to tell [herhim] how to do it afterward."
            "However, you quickly find that it is as hard as it looks, and you meekly work with [fname] as you both get your armor on collaboratively."
    scene bg training with fade
    show fprotag a at right
    show mprotag a flip at left
    friend "Wow. This feels so…weird. Even though it is small, it is a lot more weight than I was imagining. And with my bow in my hand…"
    show fprotag a happy
    friend "I feel so prepared! No monster is going to take me down!"
    show soldier at center with moveinright
    show fprotag a
    soldier "If you two are ready to go, then get over by the supply wagon. We need to assess our numbers. With all the deserters, it is doubtful that we will be providing General Ailith with any great force of aid. Those cowards…"
    hide soldier with moveoutleft
    show mprotag a
    "Muttering to himself, the soldier heads off to the wagon. You and [fname] trail behind him. Unfortunately, the force you see there is small, indeed."
    "The trainees had been few in number before, but it appears that approximately a fifth of them are missing, leaving your force looking absolutely tiny."
    show fprotag a concern at center with move
    #friend "This is it? How are we ever going to help General Ailith with these numbers?"
    menu:
        friend "This is it? How are we ever going to help General Ailith with these numbers?"
        "We have to do what we can.":
            show mprotag a flip
            hero "The General needs us. We can’t just abandon her and the rest of the troops. That would be wrong. We need to at least try to help!"
        "You’re right. What should we do?":
            show mprotag a concern flip
            hero "We have already decided that deserting is not an option. Unless you have changed your mind on that, I see only one option: we go forward and see what happens."
        "Numbers? Who cares? I was always bad at math.":
            show mprotag a happy flip
            hero "You are the one who wants to be the war hero. If you let disadvantages get in your way, how will that ever happen? Now is the perfect opportunity to show your stuff!"
    show fprotag a
    friend "I guess that you’re right. We need to get moving. Is everybody here? Then let’s go help General Ailith!"
    "You marvel at [fname] as [shehe] smoothly drops into the role of leader. Within the hour, [shehe] has everyone effectively organized, and you all head out to aid the General, with the messenger and [fname] in the lead."
    scene black with fade
    scene bg camp with fade
    "Desperatly hoping that you are not too late, you take nearly a week to get to the General’s location. And when you find the familiar military camp tents, the messenger seems surprised."
    #show soldier flip at left with moveinleft
    messenger "This isn’t where they were camped before! They must have had to fall back. Several times."
    "Worry is clearly etched into every face of your small, determined group. Wordlessly, you all increase your pace. As you reach the camp, you find that it is bustling with activity."

    show general war at right
    general "Hurry, everyone! We don’t have much time! If we want to mount a counterattack, we need to get going before nightfall! That’s when they are at their strongest!"
    "The General is so distracted by preparing to attack that she does not notice your group until the messenger runs up to her and specifically points you out. Upon seeing you, her brow creases further."
    show soldier flip at left with moveinleft
    general "This is it? And they are just the recruits from a few months ago? What about the legion stationed indefinitely at the capital?"
    messenger "Apparently the king sent them out to help another weak point. These people were the only ones there with any fighting skills at all, however small."
    "The General rubs the bridge of her nose, clearly disappointed with the help and trying to figure out how to fit you all in. Finally, she sighs, and looks directly at the small crew."
    general "I regret that I am placing you in the middle of a battle before you are ready, but unfortunately, I don’t see a choice. We need all the help we can get now."
    general "At this moment, we are preparing for a counterattack. The enemy has pushed us back too many times. We need to beat them back before they get even one step farther."
    hide general
    hide soldier
    "General Ailith proceeds to explain the rough strategy of the offense to the reinforcements."
    "The archers will hide in the trees and pick off enemies from above. You nervously leave the group with a few others and head towards the General’s other archers."
    
    show mprotag a concern flip at right
    friend "%(pname)s! Wait!"
    play sound "sounds/snd_run.wav"
    "You turn to see your friend running up to you."
    stop sound fadeout 1.0
    show mprotag a
    show fprotag a unhappy flip at left with moveinleft
    friend "I’m suddenly really nervous. What if we… you know…"
    menu:
        friend "What if… one of us…"
        "Dies? Don’t worry about it! We’ll be in the trees!":
            show mprotag a happy
            show fprotag a angry flip
            friend "%(pname)s, you are unbelievable."
            show fprotag a concern flip
            friend "I guess…that I’ll see you soon, right?"
            show fprotag a angry flip
            friend "So don’t you dare die! I’ll never forgive you!"
        "I know. But we need to do this.":
            show mprotag a concern
            show fprotag a concern flip
            friend "I know. And I feel ready. But…I just had a thought…I won’t be able to live with myself if you die. So be careful, all right?"
    show mprotag a
    hero "I will make every attempt not to die. But you better do the same!"
    show fprotag a happy flip
    friend "Of course!"
    general "Okay, troops, let’s go! This may be our only chance to prevent them from getting to the capital from here!"
    show mprotag a flip
    hide fprotag a with moveoutleft
    "[fname] gives you a quick smile before you join the group. You grip your bow and adjust your quiver, trying to force your hands not to shake."
    "The main group—with General Ailith—heads off, but as a member of the archers, you are in a smaller pack moving parallel to them, closer to the trees."
    hide mprotag with moveoutright
    scene bg forest with fade
    stop music fadeout 1.0
    "Since you are smaller and faster, you soon pull ahead of them to a location that your leader says will be close to the fighting. Everyone around you scales the trees with nimble grace, but you struggle to do the same."
    "The end result is that you do not have as much time to find a good perch in the tree, but you think that you are in a place where you can keep your balance. You pull out an arrow and fit it to your bow, waiting for the battle to start."
    "Several minutes later, you hear loud sounds of many bodies moving. The small legion has finally caught up to you. But you still don’t see the monsters. Where could they be…?"
    "Suddenly, you notice a rippling in front of the humans, and before your eyes monsters of all varieties appear. First orcs, then goblins, then trolls..."
    "They look just as hideous as you had imagined, and snarl threateningly at the humans."
    "Where did they come from? It is as if they had been invisible! However, everyone else seems to have been expecting this."
    
    general "CHARGE!!"
    scene bg battle at right
    with dissolve
    $ renpy.pause(0.5)
    show bg battle:
        xpos -1000 ypos 0 xanchor 0 yanchor 0
        linear 50.0 xpos 0 ypos 0
    play loopsound "sounds/snd_battle.MP3"
    play music "music/bgm_battle.MP3"
    "Fighting instantly erupts. Weapons clash and both people and monsters cry out from pain or exertion."
    "You try to pick out an individual target, but cannot focus on just one. The fighting is too roiling, and you cannot make sense of it."
    "Eventually, you become accustomed enough to its motions that you are able to see an orc that just took down a human."
    "Focusing carefully, you draw back your bow…and the result is lost as the battle consumes the orc once again."
    "This process continues for a while, until you see [fname] in the midst of the battle. But wasn't [shehe] supposed to be in the trees as well? Had [shehe] run out of arrows and decided to join the brawl with [herhis] small dagger?"
    "[SheHe] is holding off two orcs at once and somehow succeeding. You reach for an arrow to pick one off…and find that you have run out!"
    "Since when did that happen? You glance to your friend again, but [shehe] has disappeared."
    "You wonder what you should do, since you are out of arrows, when suddenly the decision is made for you. A nearby tussle sends an orc flying into the trunk of your tree."
    with hpunch
    play sound "sounds/snd_hit.wav"
    scene bg forest
    "A goblin breaks away from the fighting to check on the orc, but the force of the orc-tree impact has compromised your balance. You grasp at the branches, but miss every time."
    "Helpless against the pull of gravity, you fall right on top of the two creatures, hitting your head on the goblin’s wrought-iron helmet."
    with vpunch
    
    #    <SFX? Fade quickly to black, leave it for a few seconds.> 
    stop sound
    stop loopsound
    stop music
    scene black with fade
    
##############################################################################################################################################################################
label SCENE4:
    play loopsound "music/bgm_dungeon.MP3"
    scene black with dissolve
    show text "{size=+10}Chapter 4\nThe Prisoner{/size}" with dissolve
    $ save_name = "Chapter 4: The Prisoner"
    $ renpy.pause(1.5)
    scene black with dissolve
    
    scene bg dungeon with fade
    scene black with dissolve
    scene bg dungeon with fade
    show mprotag unhappy at right
    show dunfilter at right
    hero "Uurgh… What… happened? Where am I?"
    "When you finally wake up from your fall, you are in an unfamiliar place."
    "It is dark, and rather damp. There is a smell of mildew that permeates the air. You look around in confusion, and finally see the bars. And that you are in a cell."
    show mprotag angry
    hero "Huh? Wait…I’m in a dungeon?!? I was captured!"
    show mprotag concern
    "You feel panic start to take hold as you realize this. What was the outcome of the battle? Where is [fname], and General Ailith? Have they been taken captive too?"
    "You decide that you need a plan."
    menu:
        "What do you do?"
        "Look around this cell. Find some secret way out.":
            "You get up from where you had been sprawled unceremoniously on the floor to have a look."
            "Unfortunately, the bars on the door are sturdy and do not budge. The stones on the wall and the floor are not loose. You hate to admit it, but the cell you are in is very well formed. There will be no escape except out the door."
            show darklord flip at left with moveinleft
            unknown "It seems we have a curious little human here, don’t we?"
            "You look up from your investigation, startled. Since when did he arrive?"
            "And who is he? You do not remember seeing anyone like him at the battle."
        "Stay calm and take stock.":
            "The first step in figuring out a plan must be to control your fright and think with a level head. You get up from where the monsters had dumped you on the floor, and move to a straw pallet covered in mold that is obviously supposed to be where you sleep."
            "Because of your silence, you are able to hear footsteps coming from somewhere. And they are getting closer."
            show darklord flip at left with moveinleft
            unknown "Hm. I must say, I would not expect one in your situation to act so composed."
            "Even though you had heard his approach, you still don’t know what to make of this mysterious visitor."
            "What does he want from you? And who is he? You do not remember seeing anyone like him at the battle."
        "PAANIC!!! What else is there to do?":
            hero "HHHEEEEELLLLLPPPPPP!!!! SOMEBODY!!!! LET ME OUT OF HERE!!! I don’t want to die!!!!"
            "You tug on the bars keeping you captive, but they do not budge. Indeed, it seems that you may be here for a while yet. Hopefully not for the rest of your life…"
            show darklord flip annoy at left with moveinleft 
            unknown "What do you think you are trying to accomplish with that infernal racket? I demand silence!"
            "Since your wailing was so loud, you had not even heard his approach. However, part of you is glad that your yelling has annoyed him so much."
            "But...who is he? You do not remember seeing anyone like him at the battle."
    $hiding = False
    menu:
        "What do you do?"
        "\"Who are you?\"":
            show darklord flip
            unknown "How amusing. You humans wage your wars, and do not even properly know your adversaries. I suppose I shall enlighten you…"
            unknown "I am the Dark Lord Xeralin, sovereign of the peoples that you humans insist on labeling as “monsters”."
        "Spit in his face.":
            show mprotag angry
            if gender == "m":#Adjust location of mouth based on gender ;)
                show spit at Position(xpos = 0.82, xanchor=0.5, ypos=0.333, yanchor=0.5)
                show spit at Position(xpos = 0.6, xanchor=0.5, ypos = 0.35, yanchor=0.5) with move
                with Pause(1)
                show spit at Position(xpos = 0.6, xanchor=0.5, ypos = 1.0, yanchor=0.5) with move
            else:
                show spit at Position(xpos = 0.88, xanchor=0.5, ypos=0.493, yanchor=0.5)
                show spit at Position(xpos = 0.6, xanchor=0.5, ypos = 0.51, yanchor=0.5) with move
                with Pause(1)
                show spit at Position(xpos = 0.6, xanchor=0.5, ypos = 1.0, yanchor=0.5) with move
            hide spit
            "Unfortunately, your spittle is stopped mid-flight by an invisible barrier. Could this be...magic? Is this what had caused the monsters to be invisible before the battle?"
            show darklord annoy flip
            unknown "Ugh. Humans. So disgusting and typical. Did you really think that you would be able to touch me, the Dark Lord Xeralin?"
        "Hide in the shadows in the corner.":
            hide mprotag with moveoutright
            $hiding = True
            unknown "You attempts at hiding are nothing more than amusing. Please…I have already seen you. It is impossible to hide from me, the Dark Lord Xeralin, so easily."
    if not hiding:
        show mprotag concern
    "So this is the Dark Lord. You shiver as you realize your proximity to the most powerful, evil creature of all of the monsters."
    play music "music/bgm_dark.MP3"
    show darklord flip
    DarkLord "I must say, I was not expecting such an opportunity. When we repelled your kind’s pitiful attempt to push us back, we were quite surprised to find you in the aftermath. The only living human who hadn’t fled in fear."
    
    menu:
        DarkLord "And now that you have come to…well, I am sure that you know what I want."
        "What? I have no idea.":
            DarkLord "Don’t play dumb with me, human! Tell me everything you know about the battle plans your king has planned against us!"
        "Oh, no… not a tickle attack?!?":
            DarkLord "What? Just who do you think I am? No one will be tickling anyone. If you are honestly this stupid, then I don’t know how helpful this interrogation is going to be…"
        "I’m not telling you anything.":
            if not hiding:
                show mprotag angry
            DarkLord "Yes, I’m sure that you think that now. But very soon, I’m hoping that you will be rethinking your perspective. There is something rather…compelling that I think will change your mind."
    
    if not hiding:
        show mprotag concern
    
    "Just as he finishes speaking, another man comes running in. He is cradling some sort of frothing beaker in his arms. His clothes…is he some sort of mage? Or maybe an alchemist?"
    play sound "sounds/snd_run.wav"
    stop music fadeout(1.0)
    show igzalio happy flip at center with moveinleft
    show igzalio happy
    play music "music/bgm_igzalio.MP3"
    unknown "My Lord Xeralin! I have completed the thing!"
    stop sound  fadeout 1.0
    show darklord annoy flip
    DarkLord "…Thing? Igzalio, I never asked you to make any “things”."
    show igzalio
    Igzalio "Oh, but I am sure you did!! Ha ha! It is perfect! Just look! Perfection in motion!!"
    "You and Xeralin both watch as Igzalio produces a container filled with small…pills? He taps one out into his hand and drops it into the beaker. Immediately, the beaker stops bubbling."
    show igzalio happy
    DarkLord "Igzalio, explain yourself. If you hadn’t noticed, I am in the middle of something important. Your alchemy experiments are not amusing."
    show igzalio cackle
    Igzalio "But just watch!! Ha ha! The beaker represents a human’s innards…and now!! Ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha ha haaa!!!!!"
    "Suddenly, the beaker explodes, spraying glass shards and liquid everywhere."
    show igzalio happy
    Igzalio "Isn’t this wonderful, my Lord Xeralin? We can use this to make the human talk! Blowing up its insides is the most wonderful form of torture, don’t you agree?!"
    "You look at the Dark Lord in horror. He is planning to torture you to get the information he wants? By…exploding your insides?"
    show darklord angry flip
    DarkLord "Out. Now. Igzalio, how many times must I tell you? We aren’t going to torture anyone! Let’s see if the human actually cooperates first before threatening torture."
    show darklord annoy flip
    DarkLord "And…I do not think your new method will be effective. The human needs to be alive to give us information."
    show igzalio
    Igzalio "Oh. Right. I forgot. Err…let me get back to you. I need to make some modifications."
    show igzalio at Position(xpos = 0.0, xanchor=0.5, ypos=0.6, yanchor=0.5) with move
    DarkLord "Igzalio! No modifications. Get back to…something relatively more useful."
    show igzalio happy flip
    Igzalio "If you say so, my lord. However, as your advisor, I find it my job to tell you that I think you are making a mistake. A big one. Don’t blame me when I told you so! Ha!"
    show darklord annoy flip
    DarkLord "Of course. Your advice is noted. I…will consider it."
    hide igzalio with moveoutleft
    stop music fadeout(1.0)
    show darklord flip
    play music "music/bgm_dark.MP3"
    DarkLord "Pardon me, where was I?"
    
    "Struck dumb by the interaction and trying to figure out the implications, you are unable to respond."
    DarkLord "Ah, yes. I was going to ask you a question."
    menu:
        DarkLord "Why do you fight for the humans? It is a simple question, and I think that you will find that there is no information vital to the humans’ victory if you tell me."
        "Because I am a human!":
            if not hiding:
                show mprotag angry
            DarkLord "This is my point. You fight because you are human…and we fight because we are monsters. Not because we wish to take over your lands or some other ridiculous reason."
            DarkLord "Admittedly, we are making progress on several fronts, but we cannot continue our forward momentum for much longer. I only wish for peace."
            hero "Wish for peace? The monsters started this war to begin with! Why would you want to end something that you started yourself?"
            show darklord angry flip
            DarkLord "I did not start this war!"
            show darklord flip
            DarkLord "That was the doing of my father, Dark Lord Zerilan. Despite the human King Aurous’s attempt to smooth relations, my father was insistent that this war was necessary to rid the world of the vile humans."
            DarkLord "He claimed that we could not live in a world with your territory bordering ours. Now that I have taken his place, King Audric insists on continuing this war."
            DarkLord "So before raving about the moral superiority of the humans, consider just what it is you are fighting for."
        "Because you monsters are bad!":
            if not hiding:
                show mprotag angry
            DarkLord "And humans aren’t? Humans are equally capable of committing horrible crimes as we are. Being a human or a monster does not obligate you to act certain ways…such as mindlessly fighting for your own kind."
            DarkLord "I believe that you misunderstand the reasons we are fighting in this war."
            hero "What is there to misunderstand? This war was started by monsters. You are horrible creatures! All of you deserve to die!"
            DarkLord "…That is an unfortunate chapter in our history. My father, Dark Lord Zerilan, had a deep-seated hatred for the humans. The human king, King Aurous, tried to keep peace, but my father would have none of it."
            DarkLord "However, I am a different person, and my own subjects’ safety is more important to me than proving a point."
            DarkLord "King Audric wants to destroy us. My people and I are forced to fight back. He will not answer any attempts I have made at forging peace."
            DarkLord "All I ask is that you offer us some insight into Audric's plans. Think on it."
        "*Stay silent and glower*":
            if not hiding:
                show mprotag angry   
            DarkLord "All right, then, have it your way. If you do not care to cooperate with me, then I suppose that you will not listen to what I have to say."
            DarkLord "Your mind is already too far made up for me to have a chance of providing enlightenment. You must want this war to persist forever, just like that foul King Audric."
            hero "Whatever your game is, there is no way that you will get me involved. You will have to try your act on someone more gullible."
            show darklord at Position(xpos = 0.0, xanchor=0.5, ypos=0.6, yanchor=0.5)
            DarkLord "I can see that. If you are completely confident that there is absolutely nothing wrong with King Audric’s ambitions in the slightest, then please, go ahead and remain in this cell forever."
            DarkLord "I was going to ask if you would help me bring peace to both sides, rather than continuing this pointless war, but...it is quite clear that I have wasted my time by coming down here."
    hide darklord with dissolve
    if hiding:
        show mprotag concern at right behind dunfilter with moveinright
    else:
        show mprotag concern at right
    stop music fadeout 1.0
    "The Dark Lord departs, leaving you alone with your thoughts. As you cool off from your anger at meeting the monster who was keeping you captive, you ponder his words more clearly."
    "What was he trying to say? That the monsters are not the evil creatures that everyone thinks they were? And that the humans are the ones who are bad? That cannot possibly be true."
    "You think of [fname] and General Ailith. They are fighting to protect the kingdom, not because they are out for blood."
    "And as for King Audric…well, you are just a common soldier. You are not privy to his private thoughts and possible motives, and so can pass no judgement."
    scene black with fade
    "Time passes slowly from that day. You remain in your cell, listening to some far-off drip of water and watching the moss on the ceiling."
    "Occasionally you hear a few scuttling noises, but due to the dimness in the dungeons, you cannot see what causes them."
    "You have a vague sense of time outside based on when the guards bring you tasteless leftovers for meals. By your estimates, it has been four days."
    "You wonder if Dark Lord Xeralin will return to interrogate you. Maybe he is too busy plotting against the humans."
    "Bored, you again trace the walls with your eyes, looking for some weakness that you had missed before as you wait for the next meal."
    
    "Suddenly, you hear faint sounds of a fight. But why? Did some monsters get into an argument? You wouldn’t put it past them."
    play music "music/bgm_battle.MP3"
    scene bg dungeon with fade
    show mprotag concern at right
    show dunfilter at right
    "You curiously look to the dungeon entrance as it randomly bursts open. It must be that the two dungeon guards didn’t agree over something…"
    unknown "%(pname)s? Are you down here? %(pname)s!! Where are you?"
    "Is that [fname]’s voice? What is [shehe] doing here? Is it possible that there is a rescue attempt going on? For…you?"
    show mprotag happy
    "Sure enough, [fname] moves into view just beyond the doorframe, looking wildly in every direction, trying to find you. Unbidden, the Dark Lord’s words from days ago return to your mind, and you realize something."
    show mprotag concerned
    menu:
        "What do you do?"
        "[fname]!!! Over here!!":
            "There is no proof that the Dark Lord is telling the truth. In fact, he is your enemy. Most likely, his claims about King Audric were meant to unnerve you."
            "But if he was being truthful about wanting to end the war...then you need to share this information with the other humans!"
            show mprotag
            hero "I’m in here! Help!"
        "Stay silent and hide in the corner.":
            jump BRANCH1B
            
label BRANCH1A:
    friend "%(pname)s! I’m coming! I have the keys!"
    show fprotag concern flip at center with moveinleft
    "Soon, [fname] breaks away from the combat and rushes to your cell. Your friend hurriedly shoves the keys into the lock until one of them fits. With a blessed click and screech, the door opens."
    show fprotag flip at left with move
    show mprotag at center with move
    "You stumble out, weak from relief."
    friend "There are monsters everywhere. We have to be fast. Come on!"
    "Trusting [fname] to know the way out, you follow [herhim] up the spiral of stairs."
    show fprotag at left
    hide fprotag with moveoutleft
    hide mprotag with moveoutleft
    scene black with fade
    stop loopsound
    "The small group of fighters that [fname] had come in with push back the monsters as you go, ensuring your safety. As you break free of the dark castle that you had been imprisoned in, you see a horrid sight."
    "Huge camps of monsters spread in every direction. There is no possible way that General Ailith will be able to repel that many monsters with her small force, however skilled they may be."
    "The monsters are swarming everywhere, trying to get organized. The ones closest to you squeal as they notice you. You run up to [fname] to ask for a weapon…but are hit in the head before you get the chance."
    stop music
label SCENE5A:
#    <Change to Forest or On the Road BG.> 
    scene black with dissolve
    show text "{size=+10}Chapter 5\nFreedom{/size}" with dissolve
    $ save_name = "Chapter 5: Freedom"
    $ renpy.pause(1.5)
    scene black with dissolve
    
    friend "%(pname)s? %(pname)s! General! %(pname)s is awake!"
    scene bg camp with fade
    show fprotag happy flip at center with dissolve
    play music "music/bgm_army.MP3"
    "The first thing you see when you open your eyes is [fname] staring down at you excitedly. Then you notice that you are in a tent."
    show general flip at left with moveinleft
    general "How are you feeling? It must have been trying, whatever the monsters put you through."
    "Confused, you try to sit up, but a wave of dizziness rushes over you. Where…? Belatedly, you remember the events at the monster stronghold."
    show mprotag concern at right with dissolve
    hero "I…[fname]…you rescued me?"
    general "It was a glorious operation on their part. Truthfully, I had to advise [fname] against it, since the consequences of being captured were enormous."
    general "But once [shehe] had made up [herhis] mind and convinced a group of my soldiers to follow...It was impossible to dissuade them."
    friend "It was so cool! We snuck in… but then we ran into some monsters as we tried to find the dungeons, so we had to fight. I guess there isn’t much else to tell now that you don’t already know."
    hero "But what happened once we escaped the stronghold?"
    friend "Well, typical rescue stuff. We fought off the monsters and fled into the forest until we had lost them. But it was a lot harder having to lug you around, you know. I didn't realize you were that heavy."
    if gender == "m":
        show mprotag unhappy
        hero "Sorry..."
    else:# (heroine is female)
        show mprotag angry at Position(xpos = 0.6, xanchor = 0.5) with move
        play sound "sounds/snd_hit.wav"
        with hpunch
        show mprotag angry at right with move
        show fprotag concern flip
        hero "It's impolite to call a lady \"heavy\"!"
        general "..."
    
    general "I know that you are still recovering, but you must tell us about the monsters. Did you learn anything while you were imprisoned?"
    show mprotag
    hero "Well, during the escape, I saw that they had a huge force of monsters. It is much larger than our army. I do not think we can defeat it with sheer force."
    hero "But also, the Dark Lord gave me the impression that he is reluctant to fight."
    friend "Interesting. Do you think that we could use that to our advantage?"
    general "Possibly. What you said changes everything, both about the size of the army and the impressions you got from their ruler." 
    general "There may be a chance to end this before there is more bloodshed…You must see the King, so we can decide  our next course of action. I will take you."
    friend "I want to come too! I was on the rescue mission; I also saw the huge monster army. Maybe I know something important, too."
    general "Very well. I shall have three horses outfitted for us. Be prepared to ride hard."
    hide general with moveoutleft
    friend "Wow, you just woke up after getting back from the monster’s stronghold, and now we are going to the castle! To meet the king! In person!"
    menu:
        friend "It will be so cool…"
        "Yeah! I can’t wait!":
            show fprotag happy flip
            friend "I know! I wonder what he will be like…Will he act all sage and wise? That’s what I think."
        "I don’t think I’m up to this…":
            show fprotag flip
            friend "Oh, right, I guess that you should probably be resting…But you can’t pass up the opportunity to meet the king in person!"
            friend "And he needs to hear the information straight from you. Otherwise, it might get distorted by the messenger."
            hero "I suppose that you are right."
    show general flip at left with moveinleft
    general "The horses are ready. Quickly, we must leave now."
    scene black with fade
    
    "Still feeling groggy, you follow the General and [fname] out of the tent and into the bright morning. You ride hard for days before reaching the castle."

    scene bg castle with fade
    show soldier at right
    show general flip at left with moveinleft
    play music "music/bgm_regal.MP3"
    general "Finally… We must see the king immediately."
    soldier "General Ailith?!?"
    general "We have important information for the king."
    soldier "Uh...yes! Of course! Um, the king is actually in a meeting right now, discussing the décor for a gala to be held later this week, and isn’t to be disturbed… but it should be over soon."
    general "It is over NOW. We MUST speak with him IMMEDIATELY."

    "The soldier, clearly distraught at being caught between a clash of powers above his station, decides not to risk General Ailith’s wrath, and meekly brings you to the conference room."
    "Before you can crash the meeting, he flees back to his post."
    scene bg hall with fade
    show king happy flip at Position(xpos = 0.9, xanchor = 0.5)
    show general flip at Position(xpos = 0.4, xanchor = 0.5) with moveinleft
    show mprotag flip at Position(xpos = 0.25, xanchor = 0.5) with moveinleft
    show fprotag flip at left behind mprotag with moveinleft
    general "My King! I come bringing crucial information."
    king "No, no, I think that the green would look better…oh?"
    show king happy
    king "General Ailith! How nice of you to join us! What do you think: blue or green?"
    show mprotag concern flip
    show fprotag concern flip
    
    "[fname] leans over and whispers just loud enough for only you to hear."
    friend "Is he serious? This… is not at all how I imagined him to be."
    general "This meeting is over. We have sensitive information for you to hear immediately."
    king "Oh, really? How exciting! Well, then, I suppose that we shall resume this discussion later."
    show king happy flip
    king "Goodbye, men! I shall let you know when I wish to see you again!"
    "Staring at you with open curiosity, the people that the King had been meeting with depart."
    show king happy
    king "Now what is this information?"
    general "These two have been to the monsters’ stronghold. %(pname)s even came face-to-face with Dark Lord Xeralin himself."
    show king scared
    king "Oh, how scary! I’m glad that wasn’t me! But...why is this important, General?"
    general "Having been in the ENEMY’S STRONGHOLD, they have picked up some information that you may be interested in hearing."
    show king
    king "Ah, of course! So what is it?"
    show general
    "General Ailith nods to you, and you take that as an invitation to speak."
    show general at Position(xpos = 0.25, xanchor = 0.5) with move
    show general flip
    show mprotag concern flip at Position(xpos = 0.4, xanchor = 0.5) with move
    menu:
        king "So what did you learn?"
        "The monsters’ army is way too large for us to defeat with pure force.":
            friend "Yes, I saw it too, when I helped %(pname)s escape. It was positively massive."
            show king scared
            king "Just what are you trying to imply? That we humans are not skilled enough to defeat that scum?"
            general "No, my King. But I think that even if we do triumph exclusively through fighting, there will be heavy losses."
            general "Not only will that alone be saddening, but the population will wonder if defeating the monsters was worth all that death. It may make you unpopular with your subjects."
            king "So we should avoid fighting? Are you suggesting that we surrender?!?"
            general "Of course not, my King! But %(pname)s had also said something about the Dark Lord…"
            hero "Yes. To me, it seems that he was reluctant to send the monsters to fight. He might be open to a peace offering."
            king "Peace?!? I can assure you that he must be lying. Trust me, the last thing the monsters will ever want is peace. They thrive on chaos."

        "The Dark Lord says he wants peace.":
            hero "There may be a way to end this without any more fighting."
            king "But where is your proof that he was not simply lying to you? And the monstrous filth should be destroyed! If we allow it to continue, who knows what may end up happening!"
            general "I hate to bring bad news, but it seems that the size of the monster army is too massive for us to deal with through brute force."
            friend "Yeah, %(pname)s and I saw it! There were so many monsters, as far as the eye could see!"
            show king scared
            king "So do you mean that we will lose?"
            general "Do not overexcite yourself, my King. If the Dark lord will actually be receptive to a peace agreement, we can prevent any more bloodshed and will not have to engage the army."
            king "But we need to win! A “peace agreement” won’t cut it!"

    friend "Either way, it will be catastrophic for us to engage in many more fights, knowing the size of the opposing army. So we can either attempt a peace offering, or…we can try to remove the threat by the head."
    king "What is that supposed to mean?"
    show fprotag flip#Neutral Expression
    friend "Well, %(pname)s and I have some insight into the inner layout of the monster stronghold, where the Dark Lord is residing. What if we sneak in with a small group of fighters and take him out?"
    show king
    friend "Without a leader, the monsters will surely be more to open surrender."
    show king happy
    king "That is a marvelous idea! What do you think, General Ailith?"
    general "As long as you don’t get yourselves killed in the process, it sounds like a decent plan. But it could be safer to offer peace. It is really up to you, my King."
    show king
    king "Argh, how should I know what to do? You! You were captured by the monsters. What do you think we should do?"
    "General Ailith seems very displeased that King Audric is throwing the important decisions on you, but she does not speak up."
    menu:
        king "So, %(pname)s, which plan is better?"
        "The General is right. Let’s offer peace!":
            jump BRANCH5AA
        "I agree with [fname]. That will be the most effective way.":
            jump BRANCH5AB
    label BRANCH5AA:
    king "Well, if you really think that is the best choice…"
    general "If what we want is peace, then we should return to my soldiers and start preparing. If the peace talks do go south…we still need to fight."
    general "And my King…it would be wise of you to join us. If the monsters see you there, willing to talk, they will be more likely to take us seriously."
    show king scared
    king "Why? I don’t want to go! You just said that if peace talks fail, there will be a huge battle! What if I get hurt? Or WORSE!"
    general "My King, sometimes you must take risks in order to get the best outcome."
    show king
    king "Fine…I guess that I will go…"

    general "I am pleased to hear it, my King. We leave today."
    show king scared
    king "Today?!?! Don’t you think that is too soon? I need time to prepare! I need to pack all my clothes, and…"
    general "My King...Time is of the essence. We leave immediately. You have to make do with what you are wearing now."
    show king
    king "If you are sure…"
    scene black with fade
    "The General arranges for fresh horses while enduring King Audric’s continual whimpering with a steely patience. You wonder how he gained the reputation as a wise and just ruler when he is this…vacuous."
    "Maybe General Ailith and others have been guiding him all along, but he is the one who gets the credit for their wisdom."
    general "Good, here are our horses. Let’s go."
    "It takes longer to return to the camp than it did to arrive at the capital, since the King demands frequent breaks. When you finally ride into the camp, it is swarming with activity."
    scene bg camp with fade
    show soldier flip at left
    play music "music/bgm_tension.MP3"
    soldier "General Ailith! You have returned!"
    show general at center with moveinright
    show king at Position(xpos = 0.8, xanchor = 0.5) with moveinright
    show mprotag at Position(xpos = 0.7, xanchor = 0.5) with moveinright
    show fprotag at Position(xpos = 0.9, xanchor = 0.5) with moveinright
    general "Report. What is going on here?"
    soldier "Our scouts discovered activity in the monster camp. It seems they are preparing to attack…and finish us off."
    #scene bg camp with fade
    #show king flip at center
    show fprotag concern
    king "Are you still sure that they will listen to us and accept peace, Ailith? They are in the perfect position to kill me! This is horrible! Maybe you should just escort me back to the palace before it is too late…"
    show fprotag angry
    friend "No way! You are here to facilitate peace, and that is what you are going to do."
    hide soldier with dissolve
    show general at left with move
    show general flip
    general "And it seems that we will have to extend that olive branch sooner rather than later. We can prepare tonight, then approach in the morning." 
    king "Well, as long as we don’t confront them right this second… Where is my tent, by the way? I need my beauty rest. I am positively exhausted."
    general "…"
    show general
    hide general with moveoutleft
    hide king with moveoutleft
    show mprotag at left with move
    show mprotag flip
    show fprotag at right with move
    "General Ailith leaves with the king to find accommodations that meet his posh standards, and you and [fname] are alone."
    friend "Well, it looks like tomorrow will be the moment of truth, huh, %(pname)s?"
    menu:
        friend "What do you think? Will the King’s presence make this possible?"
        "Totally.":
            hero "With him here, the monsters will know that we are serious."
            show fprotag happy at right
            friend "Yes! And once they take a moment to listen, they will find that we all want the same thing. There is no way that we will fail to work out a peace agreement!"

        "Um…":
            show mprotag concern flip
            hero "Just because the King is here does not guarantee anything. We still need to back it up with terms that the monsters would find agreeable."
            show fprotag concern
            friend "I suppose that is true…"
    show fprotag
    friend "And you have had the most experience with the monsters! You should be able to figure out what they want, right?"
    show mprotag unhappy flip
    hero "I wouldn’t call that \"experience\"…"
    scene black with fade
    stop music fadeout 1.0

    "You spend the night unable to sleep, wondering how the peace talks will go in the morning. Is it possible that you will succeed? You hope so."
    
    scene bg road with fade
    show king scared at right
    show general flip at center
    show fprotag concern flip at left
    show mprotag concern flip at Position(xpos = 0.3, xanchor = 0.5)
    king "But if I hold the flag, then I will be in front, and they will shoot me! Monsters have no respect for truce flags!"
    general "But it is a symbol of trust. If we show that we trust them not to kill you, they will be more likely to trust us in return."
    king "But I don’t want to die!"
    general "This won’t kill you…"
    friend "If it is really bothering him that much, I can hold it. I want to do something important, too!"
    show king happy
    king "Yes! Great idea! We all need to divide the responsibilities evenly, don’t you think, General Ailith?"
    general "…Very well. My King, then you shall walk next to [fname]."
    king "Why can’t I go behind [herhim]?"
    general "Because the monsters actually need to be able to see you. And remember the lines you rehearsed?"
    show king happy flip
    king "Uh…yeah. We want peace! Yay! Talk with us!"
    general "…Close enough. Come on, let’s go."
    show king flip at center with move
    show general war flip at right with move
    scene black with fade
    "You nervously follow the small group. Since you are the only one who has spoken with the Dark Lord before, you are brought along to help figure out what he might be thinking."
    "You distance yourselves from the camp, with [fname] holding the white flag high in the air. When you deem yourselves close enough to the forest, King Audric speaks."
    play music "music/bgm_tension.MP3"
    scene bg forest with fade
    show king happy at Position(xpos = 0.65, xanchor = 0.5)
    show general at Position(xpos = 0.55, xanchor = 0.5)
    show fprotag concern at right
    show mprotag concern at Position(xpos = 0.75, xanchor = 0.5)
    king "Hello, er… Monsters! We want to talk! Peace! Yay! Let’s make it happen!"
    show king
    "You wince at the King’s less-than-eloquent request. And then you wait. Nothing seems to be happening in the forest."
    show king happy
    king "Maybe the monsters aren’t actually there. The scouts could have been wrong. General Ailith, do you think we should just go?"
    general "Wait. They could be figuring out how to respond."
    "You continue to stand out in the open, feeling rather self-conscious."
    show fprotag unhappy at right
    friend "Ooogh, this flag is heavy when you hold it for long periods of time…"
    general "There. I see something."
    show king
    show fprotag concern
    "You look back to the woods and discover that there is, in fact, movement, although you cannot see what is causing it. Then the Dark Lord himself emerges, along with his crazy advisor, Igzalio."
    show king scared
    king "Urk…so, that’s the Dark Lord?"
    general "My King, this will most likely go better for us if you can hide your obvious fear."
    #<King neutral. Friend, General, King, and Hero all on left, Dark Lord and Igzalio on right?>
    #show general at center
    #show king scared at center
    show darklord flip at left behind general with moveinleft
    show igzalio flip at Position(xpos = 0, xanchor = 0.5) behind darklord with moveinleft
    show king scared at Position(xpos = 0.95, xanxhor = 0.5) with move
    show mprotag concern at Position(xpos = 0.7, xanxhor = 0.5) with move
    #show fprotag at right
    #show darklord flip at left
    DarkLord "What is the meaning of this?"
    king "Er…peace? We want peace."
    DarkLord "Really? Are you sure about that? Your lack of conviction clashes with your words, Audric."
    general "We are sure. We can prevent further bloodshed, if you are willing."
    DarkLord "Why has it taken you this long to come around? I…oh, I see."
    menu:
        DarkLord "Hello, human. You are the one who escaped my dungeon."
        "Er…yes?":
            DarkLord "You humans never cease to amuse me."
        "You must be thinking of someone else.":
            DarkLord "I never forget a face. Don’t lie to me. It makes a very bad impression."
    DarkLord "Now, back to the task at hand. Negotiating peace. What are your terms?"
    general "The border will be redrawn so the humans only claim the first half-mile of the forest, rather than an entire mile, but you provide the gold to repair the human villages that the war has destroyed."
    DarkLord "I find issues with this proposal."
    general "Well then, what would you suggest?" #What do you have in mind?"
    DarkLord "You provide enough gold to rebuild all of the dwellings of my people that have been destroyed in this war. In addition, we permanently establish the border of our lands along the edge of this forest."
    DarkLord "I should be not held financially responsible for your subjects’ villages, as mine have suffered worse damage. I also suggest that we begin trading with each other."
    DarkLord "My people would greatly benefit from some of your human inventions, and I am sure there are objects of our creation that you do not have."
    show igzalio happy flip
    Igzalio "Ha ha! Yes, like my—"
    DarkLord "Silence, Igzalio."
    show igzalio flip
    king "But…that much gold…how will I be able to throw my monthly galas if we pay for all of that? And no forest? Unthinkable! And allowing monsters into my fine capital…even more unthinkable!"
    show darklord angry flip
    DarkLord "You figure it out! I have homeless citizens to worry about and you squander money on galas? Typical humans…"
    show fprotag angry
    friend "Sorry to interrupt, but you are both going to have to compromise a little if you want this to work. And we could put our own money to rebuilding human villages instead of galas."
    "Everyone looks to you. As the only one with an idea of what both sides want, you are the best person to come up with the solution."
    menu:
        "For the financial terms..."
        "The King should pay the Dark Lord…":
            #Point system to determine if the idea is balanced? Different point values for each answer for monsters and humans, if they are equal, you win, if they are unbalanced, you lose?
            $ peaceDL = peaceDL + 1
        "The Dark Lord should pay the King…":
            $ peaceKing = peaceKing + 1
        "No one should pay for any damages…":
            $ peaceKing = peaceKing + 1
            $ peaceDL = peaceDL +1

    menu:
        "For the territorial terms..."
        "…and the border should be a quarter-mile into the forest.":
            $ peaceKing = peaceKing + 1
            $ peaceDL = peaceDL +1
        "…and the border should be a half-mile into the forest.":
            $ peaceKing = peaceKing + 1
        "…and the border should go to the forest’s edge.":
            $ peaceDL = peaceDL+ 1

    if peaceKing == peaceDL:
        jump BRANCH3AAB
    elif peaceKing != peaceDL:
        jump BRANCH3AAA


    #If monsters is greater
    label BRANCH3AAA:
    if peaceKing > peaceDL:
        jump bigHumansA
    else:
        jump bigMonstersA

    label bigMonstersA:
    king "Are you out of your mind? There is no way that I am agreeing to that!"
    DarkLord "Well, if you won’t accept that, then there is no further point in this discussion. Come, Igzalio. We must prepare our attack. The humans are too unreasonable to be…well, reasoned with."
    show igzalio cackle flip
    Igzalio "Fight! Fight! Ha ha ha ha ha!!"
    jump afterTalksA
    #If humans is greater
    label bigHumansA:
    DarkLord "I thought that this negotiation was supposed to be fair."
    king "Well, it is to my eyes."
    DarkLord "You humans turn my stomach. Igzalio, we must return to our soldiers. If we can’t negotiate peace, then we will have to fight for it."
    show igzalio cackle flip
    Igzalio "Ha ha! Do not underestimate my Lord Xeralin’s wrath! Ha ha!"
    label afterTalksA:
    #<Igzalio and Dark Lord leave off side of screen. Return to Army Camp BG and move remaining characters to fill up screen?>
    stop music fadeout 1.0
    hide darklord with moveoutleft
    hide igzalio with moveoutleft
    show general
    play music "music/bgm_sad.MP3"
    general "Well, we’ve done it now. My King, you should probably return to the capital as fast as you are able. Things are going to get violent soon."
    show king scared flip
    king "That sounds like a good idea…"
    hide king with moveoutright
    show general flip
    general "And we must to prepare to fight. Hurry, get your weapons. I fear that the monsters will not leave us much time to get ready…"
    scene black with fade
    "You and [fname] hurry to get your bow and arrows. You are just about ready when you hear the beginnings of the greatest battle in which you would ever participate."
    "..."
    "However, your small contributions are not enough. The humans lose that battle, and with it, most of their best fighters. General Ailith, [fname]… It seems that everyone you knew and respected is gone."
    "You are part of a small handful of survivors from that battle, so you are able to witness the events following. The monsters run rampant through the human kingdom, destroying everything in their path."
    "The humans mount several successful counters, but none are strong enough to stop the monsters’ advance, and are all eventually destroyed."
    scene bg hall with fade
    show king flip at left with dissolve
    "The King retires to his chambers, ordering his food to be delivered to him and not ever stepping out to face the difficulties."
    scene black with fade
    "After years of continual strife, the human kingdom falls to the monsters, but by this point, there is not much left to rule anyway. And not much left of the conquerors, either."
    "Both sides have been decimated by this war, and it is doubtful that they will ever fully recover."

    jump CREDITS

    #If balanced
    label BRANCH3AAB:
    show darklord flip #neutral expression
    DarkLord "I suppose that is a fair tradeoff."
    show king #neutral expresssion
    king "I can agree to this."
    general "Well, if that’s the case…"
    show fprotag happy
    show mprotag happy
    scene black with fade
    play music "music/bgm_regal.MP3"
    "The two most powerful representatives of each kingdom proceed to work out the final details and write up a binding treaty. The process takes several long weeks."
    "You watch over the proceedings and smooth out occasional squabbles. During this time, you are unofficially declared the intermediary between the monsters and the humans."
    scene bg throne with fade
    show darklord at right
    show mprotag at center
    "Once the treaty has been finalized, you pack your things and travel to the monsters’ kingdom, where you stay in the Dark Lord’s stronghold—as a guest, not a prisoner--while you learn more about monster culture."
    show fprotag flip at left with moveinleft
    "[fname] comes with you, as a gesture of goodwill on the humans’ part, along with a few other curious minds. Monsters also travel to the human kingdom, and trade naturally starts between the two."
    "Both kingdoms thrive in this new time of peace and prosperity, and you are glad to be a force that made it happen."

    jump CREDITS

    label BRANCH5AB:
    king "A wise decision, certainly."
    friend "We need to get prepared and leave as soon as possible. We must strike before the monsters decide to attack with that huge army of theirs."
    general "You can get prepared here. I will go back to my soldiers and select a few brave souls to accompany you."
    scene bg training with fade
    "You and [fname] are given free reign over the royal armory. You predictably choose bows, as well as a large stock of arrows, but you also each take several daggers, just in case you become involved in close combat."
    "Thus prepared, you take fresh horses and ride back to the war camp."
    scene bg camp with fade
    "Once there, you find your handpicked companions waiting to go. Without wasting any time, you creep into the forest."
    play music "music/bgm_tension.MP3"
    scene bg forest at left with fade
    "After a few days of navigating the dark forest, you find yourselves at the enemy stronghold."
    scene bg evil with fade
    show mprotag a concern at Position(xpos = 0.9, xanchor = 0.5) with moveinright
    show fprotag a concern at Position(xpos = 0.7, xanchor = 0.5) with moveinright
    show orc at left with moveinleft
    "Earlier, the team decided that sleep darts would be the best way to incapacitate the stronghold guards undetected."
    play sound "sounds/snd_arrow.wav"
    show orc at Position (ypos = 1.0, yanchor = 0.0) with move
    hide orc
    "Once the guards at the door are asleep, you burst in and find…several options."
    #show fprotag a concern at Position(xpos = 0.7, xanchor = 0.5)
    show mprotag a concern flip at left with move
    show mprotag a flip concern
    show soldier at Position(xpos = 0.9, xanchor = 0.5) behind fprotag with moveinright
    friend "Uh-oh. Which direction do you think the Dark Lord will be in, %(pname)s? I know the left was where we went to get to your prison cell, so probably not that way…"
    menu:
        "Which way?"
        "Forward.":
            "The most likely place for the Dark Lord to reside is the throne room, and the most likely direction that room would be down is straight ahead."
            "You all sneak down the hall, sending guards to sleep at you go, but it is soon clear that the throne room is not here."
            "Considering [fname]’s advice, you backtrack and take the right corridor."
        "Left anyway.":
            show fprotag a angry
            friend "But I just said…Oh, never mind. If you hadn't noticed, we are on a tight schedule here..." #If you want to go and see for yourself, why not? It isn’t like we’re on a close schedule here or anything…"
            #scene black with fade
            "Ignoring [fname]’s continued muttering, you check the left corridor. However, it is soon obvious that this is not the correct way."
            friend "See? I told you so."
            "[fname] takes the lead, bringing the group back to the original crossroads."
            #scene bg human with fade
            show mprotag a unhappy flip at left
            show fprotag a at Position(xpos = 0.7, xanchor = 0.5)
            show soldier at Position (xpos = 0.9, xanchor = 0.5) behind fprotag
            friend "Hmm…eenie meeny…"
            "[fname] eventually chooses the right passage."
        "Right.":
            "Choosing one of the remaining directions at random, you decide to go right."
    "Carefully moving down the hall, you come across a large, carved door. Something tells you that it must be the door to the throne room. Will the Dark Lord be inside…? After a slight hesitation, you push open the doors…"
    play music "music/bgm_battle.MP3"
    scene bg throne with dissolve
    show darklord annoy at right
    show igzalio happy at Position(xpos = 1.0, xanchor = 0.5)
    show soldier2 flip at Position(xpos = 0.45, xanchor = 0.5) with moveinleft
    show mprotag a concern flip at Position(xpos = 0.35, xanchor = 0.5) with moveinleft
    show fprotag a concern flip at left with moveinleft
    show soldier flip at Position(xpos = 0.05, xanchor = 0.5) behind fprotag with moveinleft
    DarkLord "I was expecting something like this sooner or later."
    hero "We are taking you down, Dark Lord Xeralin!"
    friend "Yeah! Monsters won’t terrorize the humans any more once we have won!"
    DarkLord "Humans and their ideas of justice…"
    show igzalio cackle
    Igzalio "Ha ha! The humans are no match for you, my Lord Xeralin! They shall fall before us!"
    "You and [fname] both draw arrows and fit them to your bowstrings."
    show igzalio
    friend "Any last words?"
    show darklord angry
    #DarkLord "So what do you want? A surrender? I will die before allowing you to subjugate my people."
    menu:
        DarkLord "So what do you want? A surrender? I will die before allowing you to subjugate my people."
        "Surrender? Please. You have passed that option long ago.":
            jump BRANCH3ABA
        "Yes. Surrender. You do not have to die.":
            jump BRANCH3ABB
    label BRANCH3ABA:
    "You and [fname] simultaneously release your arrows."
    show igzalio at Position(xpos = 0.7, xanchor = 0.5) with moveinright
    "Igzalio tries to leap in front of the Dark Lord, but several sleep darts drop him to the floor."
    with hpunch
    play sound "sounds/snd_arrow.wav"
    show igzalio at Position(ypos = 1.0, yanchor = 0.0) with move
    hide igzalio
    "Left with very little time to react, the Dark Lord begins to make some hand motion that must be the start of a spell."
    soldier "Agh..urk...what...I don't feel so..."
    hide soldier2 flip with moveoutleft
    show mprotag a concern
    hero "No!"
    show mprotag a angry flip
    "Despite your compainion's sudden death from the Dark Lord's magic, the arrows continue on their flights. Your determination has provided them with deadly accuracy."
    show darklord
    play sound "sounds/snd_arrow.wav"
    with vpunch
    play sound "sounds/snd_arrow.wav"
    with hpunch
    "They find their marks, preventing the Dark Lord from casting any more devastating spells."
    DarkLord "Urg. You…will regret this. There could have been a better way…"
    with hpunch
    show darklord at Position(ypos = 1.0, yanchor = 0.0) with move
    hide darklord
    show fprotag a flip at right with move
    with Pause(1)
    show fprotag a happy
    play music "music/bgm_regal.MP3"
    friend "Did we…just do it? Have we actually defeated the most powerful monster? The monster army will surely scatter after this!"
    show mprotag a happy flip
    hero "Yeah…I hope so!"
    show soldier flip at left with move
    soldier "This is great! I can’t wait to tell General Ailith! We will have celebration feasts for weeks!!"
    scene black with fade
    "In the days that follow, both the humans and the monsters learn about the humans’ infiltration into the monster stronghold and the fate of the Dark Lord."
    "Igzalio is taken prisoner, and kept in a cell with no access to explosive materials. Although most of the monsters flee, a few still attack the humans, but they are easily fended off."
    "The King officially claims the lands of the monsters, and begins efforts to turn it into a more habitable environment. You and [fname] are decorated as war heroes, and live out your days as wealthy, respectable icons."

    jump CREDITS

    label BRANCH3ABB:
    hero "If you call off your army and tell the monsters to make themselves scarce and never bother the humans again, then there is no need for you to die."
    #show darklord angry
    DarkLord "Do you realize how ridiculous you sound? How would you react if I told the humans to do the same?"
    hero "That is completely different. We didn’t start this war."
    DarkLord "I didn’t start it either. It was—"
    show mprotag a angry flip
    hero "I don’t care if it was you, or your father, or some random troll in the far reaches of the land. Monsters started this war. Not humans." 
    show fprotag a angry flip
    friend "Now, if you don’t give in, we will be forced to exterminate you, and neither you nor your army will be left standing."
    show igzalio happy
    Igzalio "Ha ha! I love empty threats! You never actually know when they are empty!"
    DarkLord "And how do you figure that?"
    show igzalio
    friend "Well, once you are out of the picture, of course all of the lesser monsters will panic. How could they not? Their enemies are capable of defeating even their strongest."
    friend "And while they run in confused circles, we will wipe out the enire army with our elite forces."
    hero "But you can prevent this. Just surrender peacefully."
    show darklord
    DarkLord "I…"
    show igzalio happy
    Igzalio "My Lord Xeralin, if you do not mind me pointing it out, the ensuing chaos of our army is even more likely than the chances of success for my soldier cannons."
    show igzalio cackle
    Igzalio "I would really rather not be skewered on the end of a pointy projectile just yet. There are so many experiments that I have yet to try! Like my edible fireballs!"
    DarkLord "I understand. My subjects...I cannot let them die needlessly. I...I shall surrender. I will tell my army to stand down."
    show igzalio
    "Under the pressure of his subjects’ lives, the Dark Lord almost unconditionally surrenders everything to King Audric, with the only request that the humans be merciful to the monsters."
    show fprotag a happy flip
    show mprotag a happy flip
    friend "Wow! Peace! For real! We didn’t do what we had originally come to do, but this is just as good!"
    soldier "Wait until King Audric hears about this! Surely he will throw many galas in our honor!"
    scene black with fade
    play music "music/bgm_regal.MP3"
    "In the following days, news spreads of the humans’ victory and Dark Lord Xeralin’s surrender. The monsters at the front follow the Dark Lord’s orders and surrender as well, and the huge battle never happens."
    "For now, King Audric has given them a choice: to be exiled from both lands and to never return, or to swear fealty to him."
    "However, he cannot tolerate the idea of their hideous forms running rampant through his kingdom, and against General Ailith's stern advice, sends them to the far borders to patrol for invaders, out of sight and out of mind."
    "And so King Audric establishes his harsh, unsympathetic rule over the monsters. You have saved many lives by preventing the battle, including the lives of the monsters."
    "After surrendering to the humans, both the Dark Lord and Igzalio disappear, never to be seen again. They will never endanger any more human lives...ever."

    jump CREDITS


label BRANCH1B:
"If what the Dark Lord said is true, then King Audric may not have the best intentions at heart. What if fighting for the humans will only end up lengthening the war?"
"What if…the side you really want to be helping was the one you had been taught to hate and fear?"
hide mprotag with moveoutright
"Feeling horrible guilt, you press yourself into the dark corner and don’t answer [fname]’s calls, even as they continue to grow more desperate."
show fprotag concern flip at center with moveinleft
show soldier flip at left with moveinleft
soldier "[fname], if we stay much longer, then we will be captured as well. This rescue attempt has failed. We must escape while we still can."
show fprotag angry
friend "No! I have to find %(pname)s! We aren’t leaving until we do!"
soldier "Well, maybe you don’t mind getting caught or killed by these vermin, but I do. I’m ordering a retreat. Stay at your own risk."
show soldier
hide soldier with moveoutleft
show fprotag unhappy
stop music fadeout 1.0
friend "I guess…that I’ll be more helpful getting out of here…"
show fprotag unhappy at left with move
play music "music/bgm_sad.MP3"
show fprotag unhappy flip with Pause(1)
"Casting one crushed glance over [herhis] shoulder, [fname] retreats with the rest of the infiltration squad. You watch wordlessly, hardly daring to breath."
show fprotag unhappy
hide fprotag with moveoutleft
show mprotag unhappy at right behind dunfilter with moveinright
"Poor [fname]…But you have made your choice. Now you are on opposite sides of the war. All you can do now is wait for the Dark Lord to grow interested enough to return, so you can offer to aid him."

label SCENE5B:
#    <Fade to black, return with same BG but also Dark Lord, annoyed?>
    scene black with dissolve
    show text "{size=+10}Chapter 5\nThe Conspirator{/size}" with dissolve
    $ save_name = "Chapter 5: The Conspirator"
    $ renpy.pause(1.5)
    scene black with dissolve
    
    "Luckily enough, that did not take long. Within a day, he has come back to see you."
    scene bg dungeon with fade
    show mprotag concern at right
    show dunfilter at right
    show darklord flip at left with moveinleft
    DarkLord "I most certainly do not understand humans. Why are you so fickle? A small force manages to penetrate the castle—annoying enough—but then have the luck to make it all the way down to your cell."
    DarkLord "However, since you are still here, I take it that you either did not want to be found, or that the humans who infiltrated my defenses are exceedingly dim-witted."
    DarkLord "And that I highly doubt, especially considering that they all made it out again. So enlighten me as to why you are still here."
    hero "I suppose… that I had a realization. During the days that I spent alone in my cell, all I had were my thoughts to keep me company."
    show mprotag unhappy
    hero "The more I thought back, the more I realized that I really have no idea what King Audric wants out of this war."
    hero "When I was recruited to fight in the King’s Army, he gave a speech that made little sense and kept repeating how the monsters deserved to die."
    hero "He never backed up his claims with any hard evidence, and I guess on some level that I was suspicious all along, even though I did not realize it."
    show mprotag concern
    hero "That is, until they came to rescue me. At that moment, I became aware that I had the opportunity to choose which side I would fight for…and I sided with you."
    DarkLord "…I must admit that however sincere you may seem, I cannot discount the possibility of you simply staying here to spy, and report back to Audric’s forces when you get the chance."
    DarkLord "Unfortunately, desperate times call for desperate measures."
    #DarkLord "I see no choice but to trust you for the moment. But I warn you now: if I have even a second of doubt about you, the best case scenario for you will be returning to this cell for the rest of your life."
    menu:
        DarkLord "I see no choice but to trust you for the moment. But I warn you now: if I have even a second of doubt about you, the best case scenario for you will be returning to this cell for the rest of your life."
        "Wait, what?!? No, no no no no no.":
            DarkLord "Well, then, I guess that you weren’t serious. Pity."
            show darklord at Position(xpos = 0.0, xanchor=0.5) with move
            "He turns to leave, and you realize that you are missing your chance."
            hero "Wait! No, I didn’t mean that, I just…was startled, is all. Of course that will be fine. I…am not used to such blatant…statements of harsh facts, I suppose."
            show darklord annoy flip at left
            DarkLord "If you are quite sure this time... I will not have any hysterical breakdowns from you if you find that you can’t handle the truth."
        "That makes sense. I accept.":
            DarkLord "So you finally see reason. I was not quite sure that you had the capability."
        "Sounds fun! Let’s go!!":
            show darklord annoy flip
            DarkLord "…I will never comprehend the human mind."
    
    "Extracting a key from his robes, the Dark Lord releases you from that awful cell."
    show darklord at Position(xpos = 0.1, xanchor = 0.5)
    show darklord at left with move
    pause (0.5)
    show darklord at Position(xpos = 0.0, xanchor=0.5) with move
    show mprotag concern at center with move
    hide darklord with moveoutleft
    hide mprotag with moveoutleft
    stop music fadeout(1.0)
    stop loopsound fadeout(1.0)
    scene black with dissolve
    "You hurriedly follow him as he leads the way up a spiral flight of stairs, then through the wide stronghold corridors. Shortly, you end up in the throne room, where an unfortunately familiar face awaits."
    show bg throne
    play music "music/bgm_igzalio.mp3"
    show igzalio happy flip at left
    Igzalio "My Lord Xeralin! You have returned!!"
    show darklord at right with moveinright
    hide igzalio
    show igzalio happy flip at left     
    show mprotag at right with moveinright
    DarkLord "I was only gone for five minutes."
    show igzalio cackle flip
    Igzalio "But anything can happen in five minutes!! Ha ha ha ha ha ha!!! Such as this glorious new idea that I have concocted for our assault plan!"
    "Igzalio continues to cackle, and seems completely oblivious to your presence."
    show darklord annoy
    DarkLord "Igzalio! Just share your plan already. You know that we need to keep a tight schedule to stay ahead of King Audric’s forces!"
    show igzalio flip
    Igzalio "So we send out our forces like we were planning…"
    show igzalio happy flip
    Igzalio "But we teach them how to dance first! Then they can confuse the enemy with pirouettes!! Isn’t it absolutely brilliant, my Lord Xeralin?"
    DarkLord "I thought that I had already established last week that this would be completely irrelevant."
    show igzalio happy flip
    Igzalio "But you forget, my Lord Xeralin, that that plan was to teach them just the can-can. This idea of mine is much more all-inclusive and non-discriminating." 
    DarkLord "Well, I still have the same opinion. Now if we are finally past that…"

    show igzalio happy flip
    Igzalio "But wait! I have the perfect thing to replace the dancing with, if that is what you do not like! What about water polo?"
    show darklord angry
    DarkLord "Enough, Igzalio! Why Father left you as my advisor, I shall never understand."
    show igzalio flip
    "Igzalio looks a bit put out by having his ideas dismissed so quickly. He continues to pester the Dark Lord with equally ridiculous ideas, but is ignored as the Dark Lord turns his attention to you."
    stop music
    play music "music/bgm_dark.mp3"
    show darklord flip at Position(xpos = 0.99, xanchor = 0.5)
    show darklord flip at right with move
    #<Maybe have Igzalio’s facial expression change throughout the conversation? Or he just disappears.> 
    DarkLord "So you are offering to help us. What, exactly, can you bring to the table, besides… dancing and water polo? I already have one nincompoop to deal with—I don’t need another."
    label loopingMenu:
    menu:
        "What can you do?"
        "I can shoot stuff." if shoot == False:
            show mprotag
            show igzalio happy flip
            DarkLord "Yes, you did have a bow when we found you, now that I recall. However, I already have an army that can shoot arrows just fine. You would hardly make any difference there."
            $ shoot = True
            jump loopingMenu
        "I am a human—I have knowledge of their ways.":
            show mprotag concern
            show darklord flip
            show igzalio flip
            DarkLord "Yes…I suppose that is an advantage. What can you tell me about their offensive plans?"
            hero "You mean…when and where they will attack?"
            DarkLord "Yes, exactly! What can you share?"
            hero "Um…I don’t know any of that."
            show darklord angry flip
            DarkLord "How can you not?!? Even as a lowly archer with no station, you should at least know where you are going next!"
            hero "I was part of a group of reinforcements. I didn’t actually know anything until soon before we left to attack. And before that, I was still training to be a soldier. I never even finished."
            DarkLord "So you are telling me…that of all the humans we fought, the only one we could capture is completely useless?"
            show mprotag unhappy
            hero "…I want to help, though. Please, what can I do?"
            show darklord flip
            DarkLord "Well, without their strategies, we still don’t know what to expect. Except…the front that you fought on has suddenly gotten many more soldiers, more than tripling the number, at least according to our scouts."
            DarkLord "So it is reasonable to expect that they are planning one large assault in an attempt to break us."
            DarkLord "While my soldiers vastly outnumber the humans, we do not have access to the same quality of weapons and armor, making each individual weaker than a single human overall."
            DarkLord "With this in mind, I judge that my soldiers will barely be able to repel them. I would like a solution that spares the lives of my subjects from this needless slaughter, but I cannot see any other alternatives…"
            show mprotag concern
            hero "Is there any way we can reason with them?"
            menu:
                DarkLord "Don’t you think that I have already tried that? I don’t want this war, King Audric does."
                "Then we need to eliminate King Audric.":
                    jump BRANCH2BA
                "But what about the human generals?":
                    jump BRANCH2BB
        "I know—Lacrosse." if lacrosse == False:
            show igzalio happy flip
            show darklord angry flip
            show mprotag happy
            DarkLord "Your attempts at humor are not amusing! Do you really want to go back to the dungeons?"
            $ lacrosse = True
            jump loopingMenu 

label BRANCH2BA:
    DarkLord "Explain."
    show mprotag #neutral
    hero "Well, if King Audric is the one who wants to fight, then it makes sense that we remove him. If we can take him out of the picture before this decisive battle, then lives will be spared and you will be victorious, right?"
    DarkLord "Well, yes. But if it was that simple, it would have been done already. We can’t sneak past the enemy lines to… Wait…I see what you are suggesting."
    hero "Let me be the one to do it. I know the land better than any of you, and I also look the part. As long as no one recognizes me as the one who disappeared after the battle, I should be able to blend in."
    hero "I could take a small force of monsters with me and help them remain unnoticed. Although I admit that I am unfamiliar with the inside of the castle, I have a better idea of where to find the king than anyone else here."
    DarkLord "This…is a good point. I must admit that it is a more feasible plan than any that have been considered before."
    show darklord annoy at left
    "The Dark Lord looks sharply over to Izgalio."
    show darklord flip at right
    DarkLord "The only issue I see is how we will maintain our hold over the human capital until the battle that is brewing is dismantled. Only then will I be able to come myself and declare the war over."
    DarkLord "You will need to bring someone of high rank from our side, whose presence will be significant…and there is only one person who fits that description besides me."
    show darklord at left
    "Again, the Dark Lord’s eyes stray over to Igzalio."
    show igzalio happy flip
    Igzalio "Am I allowed back into the conversation? Because I came up with a wonderful plan to poison King Audric with teleporting burritos…"
    "So Igzalio, the Dark Lord’s advisor, would be one of the ones to come with you?"
    menu:
        "That seems…"
        "It makes sense.":
            show mprotag
            show darklord flip at right
            DarkLord "Yes, it does."
        "You just don’t want to deal with him! That isn’t fair!":
            show mprotag angry
            show darklord flip at right
            DarkLord "…For a human, you are rather perceptive sometimes."
    DarkLord "So it is a plan. I will send for a small team of our best stealth fighters to join you. You shall leave at nightfall."
    show darklord at left
    DarkLord "Servants! Help Igzalio and this human prepare for a covert operation into the human capital."
    show orc at left with moveinleft
    "Servant" "Yes, my Lord Xeralin."
    show darklord flip at right
    hide darklord with moveoutright
    show igzalio happy at center with move
    stop music fadeout 1.0
    "The servants lead you and Igzalio out of the Dark Lord’s presence."
    show orc flip
    show igzalio cackle
    play music "music/bgm_igzalio.MP3"
    Igzalio "Ha ha ha ha ha ha ha ha!! This mission is fascinating! I told my Lord Xeralin that I needed more field experience, and he has finally listened at last! Ha ha ha ha!"
    show mprotag angry
    menu:
        Igzalio "Now I can test out my teleporting burritos and torture pills on real humans!!! Ha ha ha ha ha haaaaa!"
        "There will be NO testing of experiments.":
            show igzalio flip
            Igzalio "WHAT?!? But then what is the point? Only going to kill the human king is so dumb."
        "Is it completely impossible for you to keep your mouth shut?":
            show igzalio happy flip
            Igzalio "I believe that I do not understand your question. Why would I even want to shut my mouth?"
            show igzalio happy flip
            Igzalio "There are so many beautiful tastes in the air, like my teleporting burritos!"
            show igzalio cackle flip
            Igzalio "Ha ha ha ha ha ah ha ha ha ha!!!"
        "If you don’t follow my orders, I will tell the Dark Lord to destroy you with magic.":
            show igzalio flip
            Igzalio "One does not simply tell my Lord Xeralin to do anything. He must be convinced."
            show igzalio happy flip
            Igzalio "Besides, he cannot kill me if he wanted to, because of a loophole in one of the laws that would depose him if he took any action against me."
            show igzalio cackle flip
            Igzalio "Ha hah ha ha ha ha ha ha ha ha!"
    stop music fadeout(1.0)
    scene black with fade
    play music "music/bgm_dark.mp3"
    "Deciding that there is no point in pursuing a conversation with Igzalio, you ignore him and follow the orc into a room with your belongings taken when you were captured."
    "You gleefully pick up your bow and let the monster help you put on your light armor. As it does so, you are painfully reminded of [fname], who must be worried sick over your well-being."
    "But by taking down King Audric, you will be able to spare [herhim] the danger of participating in the huge battle that will occur if you do not intervene."
    "The hours you spend waiting to leave go slowly. The servants bring you a small pack of food, as well as arrows for your bow."
    "Eventually, the small group of monsters that you will be leading into the castle are brought to you and Igzalio."
    "They already seem to have been informed of the mission's purpose, although they give you skeptical looks. You presume that it is because you are a human who had been locked in a cell until earlier that day."
    "You begin to plan the mission, exchanging ideas for where would be the best place to slip across enemy lines. Eventually, you all agree on the details and have memorized every fallback plan."
    "Glancing out the window, you are startled to see that it is sunset, and that your team will be leaving soon."
    scene bg evil with fade
    show mprotag a concern at center
    show darklord flip at left behind mprotag with dissolve
    DarkLord "So are you prepared?"
    "Since when had he come in?"
    show mprotag a
    hero "Yes. We are ready to go."
    DarkLord "Good. I personally wanted to wish you all luck. To finally have a way to spare lives…I hope you succeed."
    show igzalio happy at right with moveinright
    Igzalio "My Lord Xeralin, I am touched."
    show igzalio cackle
    Igzalio "And may I say that I wish you and our forces luck in the coming battle? Ha ha hah ha ha ha!!"
    show darklord annoy flip
    DarkLord "You do realize that…never mind."
    show darklord flip
    DarkLord "As long as you find a way to keep Igzalio quiet, I am sure that everything else will come easily."
    hero "We will try our best."
    scene black with fade

    "Soon you and the group set out. Over the course of a few days, you are able to sneak past the enemy lines and get into the capital city of the humans. Luckily, your travels are met without incident."
    scene bg forest with fade
    show mprotag a concern flip at left
    show igzalio happy at right
    show orc flip at Position(xpos = 0.6, xanchor = 0.5)
    hero "This is it. We are going to break into the castle right now."
    Igzalio "Yay! This is so exciting!! Ha ha!"
    hero "You be quiet. Now, the easiest way into the castle would probably be through the entrance to the training ground…"
    play music "music/bgm_battle.MP3"
    scene bg training night with fade
    show soldier at right
    pause (0.75)
    show mprotag a flip at center with moveinleft
    show orc at Position(xpos = 0.45, xanchor = 0.5) behind mprotag with moveinleft
    show igzalio happy flip at left with moveinleft
    "You lead the monsters to the busy training ground. Before anyone can react, you enter in a rush, catching them off guard. The monsters fire sleeping darts at the recruits, and you make it safely to the castle."
    play sound "sounds/snd_arrow.wav"
    with hpunch
    show soldier at Position (ypos = 1.0, yanchor = 0.0) with move
    hide soldier
    show orc at right with move
    show mprotag a flip at Position(xpos = 0.9, xanchor = 0.5) with move
    show igzalio happy flip at center with move
    scene black with fade
    "You run into the side entrance, and follow the corridors until you find the grand foyer. There, you see an elaborate door that surely leads to the King's room. You push open the doors and find…"
    scene bg hall with fade
    show mprotag a flip at Position(xpos = 0.3, xanchor = 0.5) with moveinleft
    show orc at Position(xpos = 0.2, xanchor = 0.5) behind mprotag with moveinleft
    show igzalio flip at Position(xpos = 0, xanchor = 0.5) with moveinleft
    show king at Position(xpos = 0.9, xanchor = 0.5) behind mprotag with moveinright
    show mprotag a concern flip
    king "Who are you? What do you think you are doing?"
    hero "We are taking you down."
    show igzalio happy flip
    Igzalio "Yeah! Monsters win! Ha ha!"
    show king scared at Position(xpos = 0.9, xanchor = 0.4) with move
    king "What?!? Somebody, help! Invaders are trying to kill me!"
    Monster "You will find that everyone else is asleep."
    "You advance forward, trapping the frightened king on his throne and leaving him with no escape routes."
    show mprotag a concern flip at Position (xpos = 0.6, xanchor = 0.5) with move
    show orc at center with move
    show igzalio happy flip at Position (xpos = 0.3, xanchor = 0.5) with move
    show king scared at Position(xpos = 0.95, xanchor = 0.4) with move
    #king "Wait, please! Spare me! What do you want? I’ll give it to you!"
    menu:
        king "Wait, please! Spare me! What do you want? I’ll give it to you!"
        "It is too late for that." :
            jump BRANCH3BAA
        "…We want you to surrender. Call off the army." :
            jump BRANCH3BAB

label BRANCH3BAA:
"Ignoring his pitiful cries for mercy, you end his life."
play sound "sounds/snd_arrow.wav"
with vpunch
king "I…ugh…No…"
show king scared at Position (ypos = 1.0, yanchor = 0.0) with move
hide king
show igzalio cackle flip
Igzalio "We have succeeded! Ha ha ha! We have conquered the humans at long last!"
Monster "This is a day for celebration!"
stop music fadeout 1.0
scene black with fade
play music "music/bgm_dark.MP3"
"In the following days, news spreads of the monsters’ brutal victory throughout both kingdoms. However, many of the soldiers, enraged by their king’s assassination, still mount an attack on the monsters."
"Since they do not have as much support as when the king was alive, they are easily defeated. The remaining soldiers swear their loyalty to the Dark Lord."
"You have not seen [fname] or General Ailith yet, however, and you wonder if they were some of the fighters that wouldn’t surrender, or if they fled the military to live under another identity."
"The Dark Lord firmly establishes his rule over the humans, and appoints you as his advisor to human affairs."
"Hopefully the humans will learn to accept the peace that has been provided, and embrace the opportunity to live side by side with monsters."

jump CREDITS

label BRANCH3BAB:
king "Surrender? I can’t do that! We need to destroy the monsters! You are a human! Why don't you understand?"
hero "The monsters do not need to be conquered. We can live peacefully with them, but you won’t accept that."
king "No! Wait! If that is the only way you will let me live…I accept. I’ll surrender to the monsters. I will tell the soldiers to stand down. Just please don’t kill me…"
hero "Hm. I thought so. "
scene black with fade
"The king almost unconditionally surrenders everything to the Dark Lord, with the one stipulation being that he could continue to live in the comfort of the castle."
scene bg training night with fade
show igzalio cackle flip at left
show orc flip at right
Igzalio "We have succeeded! Ha ha ha! The monsters have won!"
Monster "This is a day for celebration!"
stop music fadeout 1.0
scene black with fade
play music "music/bgm_dark.MP3"
"In the following days, news spreads of the monsters’ victory and King Audric’s surrender throughout both kingdoms."
"The human soldiers on the front follow their king’s orders and surrender as well, and the huge battle never happens."
"For now, the soldiers are imprisoned in the dungeons, including General Ailith and [fname], but they will be released upon swearing their loyalty to the Dark Lord."
"Many soldiers have been granted freedom already, although [fname] and General Ailith have yet to be released."
"You visit [fname] to convince [herhim] to swear loyalty to the Dark Lord, but upsettingly, [shehe] refuses to speak with you, and it is doubtful that [shehe] will get over [herhis] feelings of betrayal."
"You comfort yourself with the knowledge that saved many lives have been saved, including [fname]'s." 
"The Dark Lord establishes his rule over the humans, and you are appointed as his advisor of human affairs."
"Everything will return to a degree of normalcy, once the humans realize that it is not so bad to live under the rule of the monsters. You have realized this; surely the rest of the humans will accept it soon."

jump CREDITS

label BRANCH2BB:
DarkLord "I doubt it. They follow King Audric, remember? They will stand by his decisions, no matter how twisted they may be."
hero "Yes, but the general that I fought under in that battle—General Ailith—seemed to be feeling the effects of the war like you have. I’m sure that if we could talk to her, she would see reason and agree with us."
DarkLord "That’s risky… If trying to talk this General Ailith into peace fails, the battle will continue, and it will likely be worse for us."
DarkLord "If we attempt to make peace, the humans may see that as a sign that we are weakening, and their morale will rise."
hero "But isn’t the chance to come to a peaceful conclusion and save lives preferable to there being no chance at all of that happening?"
DarkLord "I suppose. If you are confident that this will work…"
hero "I am."
DarkLord "Very well. But I am not going to stop preparing my army. If this fails, we will need to fight."
DarkLord "You must come out with us to help the peace talks along. They will not listen to me or Igzalio, but since you are human, you stand a better chance. We leave for the front today."
hide darklord with dissolve
"The Dark Lord goes off to prepare for departure."
show orc at center behind igzalio with moveinleft
"A servant appears with your gear, returning it after your brief captivity. You decide to bring it with you, even though you will not be wearing it at the peace talks."
play music "music/bgm_tension.MP3"
scene bg forest with fade
show darklord flip at center
show mprotag concern flip at left
"It is a new experience for you to be traveling to the front, since you were unconscious during your transportation to the monster stronghold."
"Much sooner than you were expecting, you meet up with a faction of the army. Their camp is densely packed around the trees and underbrush."
DarkLord "Igzalio should be along shortly. As soon as he arrives, we will go out into the open. Igzalio will carry the white flag; I doubt there is much else that he can do to be helpful."
show igzalio happy at right with moveinright
Igzalio "I heard you calling, my Lord Xeralin! What is it that you require?"
DarkLord "Carry this. No…the flag needs to be in the air. You can’t hold it upside down like that."
Igzalio "But this way, I can use the flag to collect—"
show darklord annoy flip
DarkLord "No. Flip it around."
show igzalio
Igzalio "Okay, if you insist…"
scene black with fade
"After the slow start, the Dark Lord leads the way to the edge of the forest. You can see the humans’ army camp stretching in every direction, like a small city of tents."
"After walking a fair distance out of the forest, Igzalio raises the flag high and waves it around crazily."
"You see some of the inhabitants of the camp stare at your group in shock, then sprint away, assumedly to tell General Ailith."
"Now you just have to wait."
"…And wait."
"Standing out in the open as you are, you feel extremely aware of the possible looks that you are gathering. After all, you are a human standing with the monsters."
"Finally, you can see a group approaching from the other side."
"You can make out the form of General Ailith, and…[fname]?!? And was that the king as well?!?!"
DarkLord "Oh great. He’s here, too."
scene bg camp with fade
show king at Position(xpos = 0.85, xanchor = 0.5)
show general at Position(xpos = 0.7, xanchor = 0.5)
show darklord flip at left with moveinleft
show igzalio flip at Position(xpos = 0.0, xanchor=0.5) with moveinleft
show mprotag flip at Position(xpos = 0.35, xanchor = 0.5) with moveinleft
#show king at right#<Friend, General Ailith, and King(happy) appear on one side opposite from hero, dark lord, and Igzalio.>
general "I hear you want to talk peace."
show king happy
king "Why? Couldn’t stand the immense skill of my superior army?"
show fprotag happy at right with moveinright
show king
friend "%(pname)s!?!? What are you doing here? After we couldn’t find you in the stronghold, I thought that you were… Never mind that!"
show fprotag unhappy
menu:
    friend "But what are you doing on their side?"
    "[fname]! It is so good to see you!":
        show fprotag
    "We can catch up later. This is much more important.":
        show fprotag concern
DarkLord "You can skip the pleasantries for now. And I presume that you are General Ailith?"
general "Yes. Let’s get straight to business. What are your terms?"################################^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^general "While we are moe skilled and better equipped than those monsters, there are many more of them than there are us. If we participated in this battle, it could go either way. But even if we won, I doubt there would be enough of us left alive to might much longer.
DarkLord "I would accept enough gold to rebuild all of the dwellings of my people that have been destroyed in this war."
DarkLord "In addition, I want the border of our lands permanently established along the edge of this forest, rather than a mile in from the edge."
DarkLord "I also suggest that we eventually establish trade with each other. My people would greatly benefit from some of your human inventions, and I am sure there are objects of our creation that you do not have."
show igzalio cackle flip
Igzalio "Ha ha! Yes, like my—"
DarkLord "Be quiet."
#hide igzalio with moveoutleft
show igzalio flip
show king scared
king "But if you get that much gold, then how will I throw all of my monthly galas? Or rebuild my own destroyed villages? That is too much! And then losing all of that land? Inconceivable!"
show igzalio happy flip
Igzalio "Then what do you propose, Mr. Fancy Pants?"
king "Er…"
general "You pay for our damages. Our villages have seen the brunt of the destruction."
general "And about the border… It could be redrawn to go in only half a mile, but we cannot give any more and still expect our villages near the forest to have the same lifestyle."
show darklord annoy flip
DarkLord "That is too needy. I won’t accept that."
show fprotag angry
friend "You do realize that you both need to compromise for this to work, right?"
"Everyone looks to you. As the only one with an idea of what both sides want, you are the best person to come up with the solution."
menu:
    "For the financial terms..."
    "No one should pay for any damages…":
        $ peaceDL = peaceDL + 1
        $ peaceKing = peaceKing + 1
    "The Dark Lord should pay the King…":
        $ peaceKing = peaceKing + 1
    "The King should pay the Dark Lord…":
        #Point system to determine if the idea is balanced? Different point values for each answer for monsters and humans, if they are equal, you win, if they are unbalanced, you lose?
        $ peaceDL = peaceDL + 1
menu:
    "For the territorial terms..."
    "…and the border should go to the forest’s edge.":
        $ peaceDL = peaceDL + 1
    "…and the border should be a half-mile into the forest.":
        $ peaceKing = peaceKing + 1
    "…and the border should be a quarter-mile into the forest.":
        $ peaceDL = peaceDL + 1
        $ peaceKing = peaceKing + 1

if peaceKing == peaceDL:
    jump BRANCH3BBB
else:
    jump BRANCH3BBA

#if unbalanced
label BRANCH3BBA:

if peaceKing > peaceDL:
    jump bigHumansB
else:
    jump bigMonstersB

#If monsters is greater
label bigMonstersB:
king "Are you out of your mind? There is no way that I am agreeing to that!"
general "My King, value the chance for peace. If you would reconsider..."
king "No! That traitor human clearly favors the monters with those terms!"
DarkLord "Well, if you won’t accept that, then there is no further point in this discussion."
general "Very well, then. Come, [fname], my King. We must prepare our attack." # Our enemies apparently can’t be reasoned with."
jump afterTalksB
#If humans is greater
label bigHumansB:
show king happy
DarkLord "…You do realize that I still have basic conditions I want met? You are supposed to be helping my side, human."
king "I think that sounds agreeable."
DarkLord "Well, I do not."
show mprotag unhappy flip
hero "Sorry, I tried..."
general "If you cannot even come to a consensus amongst yourselves, then it seems we have wasted our time here."
label afterTalksB:
show fprotag unhappy
hide king with moveoutright
hide general with moveoutright
with Pause(1)
hide fprotag with moveoutright
play music "music/bgm_sad.MP3"
scene bg forest with fade
show darklord annoy at center
show igzalio cackle at right
show mprotag unhappy flip at left
Igzalio "Fight! Fight! Ha ha ha ha ha!!"
DarkLord "This is what I feared…but it was a calculated risk. Prepare yourself."
scene black with fade
"You hurry to get your weapon. You are just about ready when you hear the beginnings of the greatest battle in which you would ever participate. However, your small contributions are not enough."
scene bg battle at Position(xpos = 1.0, xanchor = 1.0) with fade
"It seems that the Dark Lord was correct when he worried the humans' morale would increase upon the monsters' peace offer. The monsters lose that battle, and with it, most of their best fighters."
scene black with dissolve
"But the humans also lost many fighters…[fname] and General Ailith are gone."
"You are part of a small handful of survivors from that battle, so you are able to witness the events following."
"The remaining humans, without General Ailith's guiding hand, run rampant through the monster kingdom, destroying everything in their path."
"King Audric redoubles the recruitment efforts to keep the human soldiers plentiful."
"The monsters mount several successful counters, but with a rapidly shrinking supply of resources, none are strong enough to stop the humans’ advance, and are all eventually destroyed."
"Even the Dark Lord himself tries to stop the humans directly, and he is successful…but there is only so long that one can stand against many."
scene bg throne with fade
"Igzalio disappears, and you have no idea if he fled or fought to save the monsters, or if it was due to a failed experiment…"
"After years of this continual strife, the monster kingdom falls to the humans, but by this point, there is not much left to rule anyway."
"And not much left of the conquerors, either. Both sides have been decimated by this war, and it is doubtful that they will ever fully recover."

scene black with fade
jump CREDITS

#If balanced
label BRANCH3BBB:
show darklord flip
DarkLord "I suppose that is a fair tradeoff."
king "I can agree to this."
show mprotag flip happy
show fprotag happy
general "Well, if that’s the case…"
play music "music/bgm_regal.MP3"
scene black with fade
"The two most powerful representatives of each kingdom proceed to work out the final details and write up a binding treaty. The process takes several long weeks."
"You watch over the proceedings and smooth out occasional squabbles. During this time, you are unofficially declared the intermediary between the monsters and the humans."
scene bg throne with fade
show darklord at right
show mprotag flip at center
"Once the treaty has been finalized, you pack your things and travel to the monsters’ kingdom, where you stay in the Dark Lord’s stronghold—as a guest, not a prisoner--while you learn more about monster culture."
show fprotag flip at left with moveinleft
"After you explain your reasons for helping the monsters, [fname] comes with you, as a gesture of goodwill on the humans’ part, along with a few other curious minds. Monsters also travel to the human kingdom, and trade naturally starts between the two."
"Both kingdoms thrive in this new time of peace and prosperity, and you are glad to be a force that made it happen."

jump CREDITS


label CREDITS:
    scene bg dash with fade
    $ renpy.pause(3)
    scene bg credits with fade
    $ renpy.pause(3)


return